/*
 ******************************************************************************
 * @file    lsm6dsm_ex.h
 * @author  Universal Driver Development Team - villanif
 * @brief   lsm6dsm expansion functions: This file contains expansion function 
 *              that help with reading, conversion of measurements and 
 *              advanced configurations
 ******************************************************************************
 * @attention
 *
 * Beautiful license
 *
 ******************************************************************************
 */

#ifndef LSM6DSM_EX_H
#define LSM6DSM_EX_H

#ifdef __cplusplus
extern "C" {
#endif

#include "lsm6dsm.h"

/*! @defgroup lsm6dsm_XL_GY_functions Group: XL GY functions  
*  lsm6dsm accelerometer and gyroscope functions
*  @{
*/

/**
 * @brief Calculate the accelerometer value in mG from the value stored in the 
 *        accelerometer registers in configs->reg.  
 * 
 * @param configs         user defined lsm6dsm_t struct (ptr)
 * @param xyz             pointer to storage buffer of type float and size 3 to store the calculated data 
 * @return lsm6dsm_err_t  error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_calc_accel_data_mG(lsm6dsm_t *configs, float * xyz);

/**
 * @brief Calculate the Gyroscope value in dps from the value stored in the 
 *        gyroscope registers in configs->reg.  
 * 
 * @param configs         user defined lsm6dsm_t struct (ptr)
 * @param xyz             pointer to storage buffer of type float and size 3 to store the calculated data 
 * @return lsm6dsm_err_t  error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_calc_gyro_data_dps(lsm6dsm_t *configs, float * xyz);

/**
 * @brief Read the Accelerometer data from the sensor, store the raw data in the user struct and store the
 *        calculated data in the xyz buffer
 * 
 * @param configs         user defined lsm6dsm_t struct (ptr)
 * @param xyz             pointer to storage buffer of type float and size 3 to store the calculated data 
 * @return lsm6dsm_err_t  error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_get_accel_data(lsm6dsm_t *const configs, float * xyz);

/**
 * @brief Read the Gyroscope data from the sensor, store the raw data in the user struct and store the
 *        calculated data in the xyz buffer
 * 
 * @param configs         user defined lsm6dsm_t struct (ptr)
 * @param xyz             pointer to storage buffer of type float and size 3 to store the calculated data 
 * @return lsm6dsm_err_t  error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_get_gyro_data(lsm6dsm_t *const configs, float * xyz);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif /* LSM6DSM_EX_H */
