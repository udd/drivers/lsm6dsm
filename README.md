# README

## Example code for STM 

```c
/* USER CODE BEGIN Includes */
#include "lsm6dsm.h"
/* USER CODE END Includes */
...
...
lsm6dsm_t lsm6dsm = LSM6DSM_DEFAULT_REG_VALUES;
...
void lsm6dsm_read(uint8_t reg, uint8_t *buf, uint8_t bytes);
void lsm6dsm_write(uint8_t reg, uint8_t *buf, uint8_t bytes);

int main(void){
  lsm6dsm.reg.ctrl3_c.sim = LSM6DSM_3_WIRE_SPI; // enable 3-wire spi

  lsm6dsm.comm.read = lsm6dsm_read;  // void spi3_read(uint8_t reg, uint8_t *buf, uint8_t bytes);
  lsm6dsm.comm.write= lsm6dsm_write; //  void spi3_write(uint8_t reg, uint8_t *buf, uint8_t bytes);
  
  lsm6dsm_setup_communication(&lsm6dsm); //setup communication, check return value to confirm functionality

  lsm6dsm.reg.ctrl1_xl.odr_xl = LSM6DSM_XL_ODR_12Hz5; //set accelerometer frequency
  lsm6dsm_write_reg(&lsm6dsm, LSM6DSM_CTRL1_XL);   //update sensor
  while (1)
  {
	  float xyz[3] = {0};
	  lsm6dsm_read_reg(&lsm6dsm, LSM6DSM_STATUS_REG); //update struct

	  if(lsm6dsm.reg.status_reg_status_spi.xlda){  //check new data available

		  lsm6dsm_read_regs(&lsm6dsm, LSM6DSM_OUTX_L_XL, 6); //read data
		  lsm6dsm_calc_accel_data_mG(&lsm6dsm, xyz); //convert data to float
		  
	  }
}
...
...
...
void lsm6dsm_read(uint8_t reg, uint8_t *buf, uint8_t bytes){
/* Set CS pin low, sensor enters SPI mode, lps22hb selected for communication */
	HAL_GPIO_WritePin(CS_AG_GPIO_Port, CS_AG_Pin, GPIO_PIN_RESET);
	reg|=0x80;
	/* Write register to be read to the sensor, masked with 0x80 indicating read command */
	HAL_SPI_Transmit(&hspi2, &(reg), 1, 1000);
	HAL_SPI_Receive(&hspi2, buf, bytes, 1000);


	/* Set CS pin high, free 3-wire SPI for other communication */
	HAL_GPIO_WritePin(CS_AG_GPIO_Port, CS_AG_Pin, GPIO_PIN_SET);
}
void lsm6dsm_write(uint8_t reg, uint8_t *buf, uint8_t bytes){
	HAL_GPIO_WritePin(CS_AG_GPIO_Port, CS_AG_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi2, &reg, 1, 1000);
	HAL_SPI_Transmit(&hspi2, buf, bytes, 1000);
	HAL_GPIO_WritePin(CS_AG_GPIO_Port, CS_AG_Pin, GPIO_PIN_SET);
}
```

