/*
 ******************************************************************************
 * @file    lsm6dsm.h
 * @author  Universal Driver Development Team - Federico Villani
 * @brief   lsm6dsm header: This file contains the memory-mapped register
 *                          struct, communication and test function prototypes
 ******************************************************************************
 * @attention
 * 
 *  Copyright (c) 2021 UDD Team - Federico Villani. 
 *  All rights reserved.
 *  This work is licensed under the terms of the MIT license.  
 *  For a copy, see <https://opensource.org/licenses/MIT> or
 *  appended LICENSE file.
 *  
 ******************************************************************************
 */
#ifndef LSM6DSM_H
#define LSM6DSM_H

#ifdef __cplusplus
extern "C" {
#endif

#include "lsm6dsm_reg.h"
#include "lsm6dsm_default.h"

/*! @defgroup lsm6dsm_I2C_defines Group: I2C defines   
*  lsm6dsm defines 
*  @{
*/

/* Read and write adresses */
#define LSM6DSM_I2C_ADD_L 0x6AU /**< Pin 1 connected to GND **/
#define LSM6DSM_I2C_ADD_H 0x6BU /**< Pin 1 connected to VDD **/
/* Device address  */
#define LSM6DSM_I2C_ADD LSM6DSM_I2C_ADD_L /* Default I2C address is low */

#define LSM6DSM_I2C_ADD_READ  ((LSM6DSM_I2C_ADD << 1) | 1)   /**< I2C Read address **/
#define LSM6DSM_I2C_ADD_WRITE (LSM6DSM_I2C_ADD << 1)         /**< I2C Write address **/

/*! @} */

/*! @defgroup lsm6dsm_error_codes Group: error codes   
*  lsm6dsm error codes
*  @{
*/

/* Error types */
typedef uint8_t lsm6dsm_err_t;
#define LSM6DSM_SUCCESS         (lsm6dsm_err_t)(0)
#define LSM6DSM_ERROR           (lsm6dsm_err_t)(1)
#define LSM6DSM_ERR_CONFIG      (lsm6dsm_err_t)(1 << 1)
#define LSM6DSM_ERR_RESPONSE    (lsm6dsm_err_t)(1 << 2)
#define LSM6DSM_ERR_VALUE       (lsm6dsm_err_t)(1 << 3)
#define LSM6DSM_NO_NEW_DATA     (lsm6dsm_err_t)(1 << 4)
#define LSM6DSM_ERR_COMM        (lsm6dsm_err_t)(1 << 5)

/*! @} */

/* Device properties data  */
#define LSM6DSM_WHO_AM_I_VAL (uint8_t)0x6AU /**< Device Who am I value **/

/* Bitmask */
#define LSM6DSM_00_1F_READ_BITMASK 0x7FFFFEF2U  /**< read bitmask for bytes 0..31 **/
#define LSM6DSM_00_1F_WRITE_BITMASK 0x07FF6FF2U /**< write bitmask for bytes 0..31 **/

#define LSM6DSM_20_3F_READ_BITMASK 0xFFFFFFFFU  /**< read bitmask for bytes 32..63 **/
#define LSM6DSM_20_3F_WRITE_BITMASK 0x00000000U /**< write bitmask for bytes 32..63 **/

#define LSM6DSM_40_5F_READ_BITMASK 0xFF3FFE07U  /**< read bitmask for bytes 64..95 **/
#define LSM6DSM_40_5F_WRITE_BITMASK 0xFF000004U /**< write bitmask for bytes 64..95 **/

#define LSM6DSM_60_7F_READ_BITMASK 0xFF3FFE07U  /**< read bitmask for bytes 96..127 **/
#define LSM6DSM_60_7F_WRITE_BITMASK 0xFF000004U /**< write bitmask for bytes 96..127 **/

#define LSM6DSM_REG_SIZE 0x80U /**< number of registers of the LSM6DSM sensor **/


#define LSM6DSM_SCT_TO_P(STRUCT) (uint8_t *)&(STRUCT)
#define LSM6DSM_SCT_TO_B(STRUCT) *LSM6DSM_SCT_TO_P(STRUCT)
#define LSM6DSM_SCT_REG_ADD(STRUCT, MEMBER_ADDRESS) LSM6DSM_SCT_TO_P(STRUCT->reg) + MEMBER_ADDRESS
#define LSM6DSM_NULL_PNT(PNT) PNT == 0

#define LSM_CHECK_REG_SIZE_COMPILE_TIME(REG, SIZE) ((void)sizeof(char[1 - 2*(sizeof(REG) != SIZE)]))


/*! @defgroup lsm6dsm_reg_comm_struct Group: reg comm struct  
*  lsm6dsm register map and communication structs 
*  @{
*/
typedef struct
{
  uint8_t const reserved_0h;                                 /**< 0x00 **/
  lsm6dsm_func_cfg_access_t func_cfg_access;                 /**< 0x01 r/w **/
  uint8_t const reserved_2h_3h[2];                           /**< 0x02 - 0x03 **/
  lsm6dsm_sensor_sync_time_frame_t sensor_sync_time_frame;   /**< 0x04 r/w **/
  lsm6dsm_sensor_sync_res_ratio_t sensor_sync_res_ratio;     /**< 0x05 r/w **/
  lsm6dsm_fifo_ctrl1_t fifo_ctrl1;                           /**< 0x06 r/w **/
  lsm6dsm_fifo_ctrl2_t fifo_ctrl2;                           /**< 0x07 r/w **/
  lsm6dsm_fifo_ctrl3_t fifo_ctrl3;                           /**< 0x08 r/w **/
  lsm6dsm_fifo_ctrl4_t fifo_ctrl4;                           /**< 0x09 r/w **/
  lsm6dsm_fifo_ctrl5_t fifo_ctrl5;                           /**< 0x0A r/w **/
  lsm6dsm_drdy_pulse_cfg_t drdy_pulse_cfg;                   /**< 0x0B r/w **/
  uint8_t const reserved_Ch;                                 /**< 0x0C **/
  lsm6dsm_int1_ctrl_t int1_ctrl;                             /**< 0x0D r/w **/
  lsm6dsm_int2_ctrl_t int2_ctrl;                             /**< 0x0E r/w **/
  uint8_t who_am_i;                                          /**< 0x0F r **/
  lsm6dsm_ctrl1_xl_t ctrl1_xl;                               /**< 0x10 r/w **/
  lsm6dsm_ctrl2_g_t ctrl2_g;                                 /**< 0x11 r/w **/
  lsm6dsm_ctrl3_c_t ctrl3_c;                                 /**< 0x12 r/w **/
  lsm6dsm_ctrl4_c_t ctrl4_c;                                 /**< 0x13 r/w **/
  lsm6dsm_ctrl5_c_t ctrl5_c;                                 /**< 0x14 r/w **/
  lsm6dsm_ctrl6_c_t ctrl6_c;                                 /**< 0x15 r/w **/
  lsm6dsm_ctrl7_g_t ctrl7_g;                                 /**< 0x16 r/w **/
  lsm6dsm_ctrl8_xl_t ctrl8_xl;                               /**< 0x17 r/w **/
  lsm6dsm_ctrl9_xl_t ctrl9_xl;                               /**< 0x18 r/w **/
  lsm6dsm_ctrl10_c_t ctrl10_c;                               /**< 0x19 r/w **/
  lsm6dsm_master_config_t master_config;                     /**< 0x1A r/w **/
  lsm6dsm_wake_up_src_t wake_up_src;                         /**< 0x1B r **/
  lsm6dsm_tap_src_t tap_src;                                 /**< 0x1C r **/
  lsm6dsm_d6d_src_t d6d_src;                                 /**< 0x1D r **/
  lsm6dsm_status_reg_status_spi_aux_t status_reg_status_spi; /**< 0x1E r **/
  uint8_t const reserved_1Fh;                                /**< 0x1F **/
  uint8_t out_temp[2];                                       /**< 0x20 LSB, 0x21 MSB r **/
  uint8_t outx_gy[2];                                        /**< 0x22 LSB, 0x23 MSB r **/
  uint8_t outy_gy[2];                                        /**< 0x24 LSB, 0x25 MSB r **/
  uint8_t outz_gy[2];                                        /**< 0x26 LSB, 0x27 MSB r **/
  uint8_t outx_xl[2];                                        /**< 0x28 LSB, 0x29 MSB r **/
  uint8_t outy_xl[2];                                        /**< 0x2A LSB, 0x2B MSB r **/
  uint8_t outz_xl[2];                                        /**< 0x2C LSB, 0x2D MSB r **/
  uint8_t sensorhub_reg_1_12[12];                            /**< 0x2E SENSORHUB1_REG - 0x39 SENSORHUB12_REG r **/
  lsm6dsm_fifo_status1_t fifo_status1;                       /**< 0x3A r **/
  lsm6dsm_fifo_status2_t fifo_status2;                       /**< 0x3B r **/
  lsm6dsm_fifo_status3_t fifo_status3;                       /**< 0x3C r **/
  lsm6dsm_fifo_status4_t fifo_status4;                       /**< 0x3D r **/
  uint8_t fifo_data_out[2];                                  /**< 0x3E LSB, 0x3F MSB r **/
  uint8_t timestamp_reg[3];                                  /**< 0x40 LSB, 0x42 MSB r r r/w **/
  uint8_t const reserved_43h_48h[6];                         /**< 0x43 - 0x48 **/
  uint8_t step_timestamp[2];                                 /**< 0x49 LSB, 0x4A MSB r **/
  uint8_t step_counter[2];                                   /**< 0x4B LSB, 0x4C MSB r **/
  uint8_t sensorhub_reg13_18[6];                             /**< 0x4D SENSORHUB13_REG - 0x52 SENSORHUB18_REG r **/
  lsm6dsm_func_src1_t func_src1;                             /**< 0x53 r **/
  lsm6dsm_func_src2_t func_src2;                             /**< 0x54 r **/
  lsm6dsm_wrist_tilt_ia_t wrist_tilt_ia;                     /**< 0x55 r **/
  uint8_t const reserved_56h_57h[2];                         /**< 0x56, 0x57 **/
  lsm6dsm_tap_cfg_t tap_cfg;                                 /**< 0x58 r/w **/
  lsm6dsm_tap_ths_6d_t tap_ths_6d;                           /**< 0x59 r/w **/
  lsm6dsm_int_dur2_t int_dur2;                               /**< 0x5A r/w **/
  lsm6dsm_wake_up_ths_t wake_up_ths;                         /**< 0x5B r/w **/
  lsm6dsm_wake_up_dur_t wake_up_dur;                         /**< 0x5C r/w **/
  lsm6dsm_free_fall_t free_fall;                             /**< 0x5D r/w **/
  lsm6dsm_md1_cfg_t md1_cfg;                                 /**< 0x5E r/w **/
  lsm6dsm_md2_cfg_t md2_cfg;                                 /**< 0x5F r/w **/
  lsm6dsm_master_cmd_code_t master_cmd_code;                 /**< 0x60 r/w **/
  lsm6dsm_sens_sync_spi_errc_t sens_sync_spi_errc;           /**< 0x61 r/w **/
  uint8_t const reserved_62h_65h[4];                         /**< 0x62 - 0x65 **/
  uint8_t out_mag_raw_x[2];                                  /**< 0x66 LSB, 0x67 MSB r **/
  uint8_t out_mag_raw_y[2];                                  /**< 0x68 LSB, 0x69 MSB r **/
  uint8_t out_mag_raw_z[2];                                  /**< 0x6A LSB, 0x6B MSB r **/
  uint8_t const reserved_6Ch_6Eh[3];                         /**< 0x6C - 0x6E**/
  lsm6dsm_int_ois_t int_ois;                                 /**< 0x6F r/w **/
  lsm6dsm_ctrl1_ois_t ctrl1_ois;                             /**< 0x70 r/w **/
  lsm6dsm_ctrl2_ois_t ctrl2_ois;                             /**< 0x71 r/w **/
  lsm6dsm_ctrl3_ois_t ctrl3_ois;                             /**< 0x72 r/w **/
  uint8_t x_ofs_usr;                                         /**< 0x73 r/w **/
  uint8_t y_ofs_usr;                                         /**< 0x74 r/w **/
  uint8_t z_ofs_usr;                                         /**< 0x75 r/w **/
  uint8_t const reserved_76h_7Fh[10];                        /**< 0x76 - 0x7F**/
} lsm6dsm_registers_t;


typedef struct
{
  lsm6dsm_slv0_add_t slv0_add;
  lsm6dsm_slave0_config_t slave0_config;
  lsm6dsm_slv1_add_t slv1_add;
  lsm6dsm_slave1_config_t slave1_config;
  lsm6dsm_slv2_add_t slv2_add;
  lsm6dsm_slave2_config_t slave2_config;
  lsm6dsm_slv3_add_t slv3_add;
  lsm6dsm_slave3_config_t slave3_config;
  lsm6dsm_config_pedo_ths_min_t config_pedo_ths_min;
  lsm6dsm_pedo_deb_reg_t pedo_deb_reg;
  lsm6dsm_a_wrist_tilt_mask_t a_wrist_tilt_mask;
} lsm6dsm_embedded_registers;

typedef struct
{
  lsm6dsm_err_t (*write)(uint8_t reg, uint8_t *buf, uint8_t bytes); /**< User write function, should write multiple bytes **/
  lsm6dsm_err_t (*read)(uint8_t reg, uint8_t *buf, uint8_t bytes);  /**< User read function, should read multiple bytes **/
} lsm6dsm_communication_t;

typedef struct
{
  lsm6dsm_communication_t comm;
  lsm6dsm_registers_t reg;
  lsm6dsm_embedded_registers emb_reg;
} lsm6dsm_t;

/*! @} */

/*! @defgroup lsm6dsm_setup_functions Group: setup functions   
*  lsm6dsm functions
*  @{
*/

/*********** Functions declaration ***********/

/*********** Setup functions ***********/
/**
 * @brief Save the pointer of the user-defined write-read function.
 *        Checks the WHO_AM_I register and write/read. 
 * 
 * @warning If using 3-wire SPI set spi_mode variable to LSM6DSM_3_WIRE_SPI.
 * @warning Make sure reg->ctrl3_c.if_inc is enabled if the MCU function used 
 *          assumes that the register address increases automatically.
 * 
 * @param configs           user defined setup struct (ptr)
 * @param spi_mode          set spi mode. Default: LSM6DSM_4_WIRE_SPI
 * @return lsm6dsm_err_t    error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_setup_communication(lsm6dsm_t *const configs, lsm6dsm_sim_t spi_mode);

/**
 * @brief Write the user configurations in the lsm6dsm sensor, by iterating
 *        over all configuration registers
 * 
 * @param configs        user defined lsm6dsm_t struct (ptr)
 * @return lsm6dsm_err_t error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_setup(lsm6dsm_t *const configs);

/**
 * @brief Check that the user configuration stored in the chip is
 *        the same as the configuration stored in the user setup struct.
 * 
 * @param configs         user defined lsm6dsm_t struct (ptr)
 * @return lsm6dsm_err_t  error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_setup_check(lsm6dsm_t *const configs);

/**
 * @brief Check the WHO AM I register
 * 
 * @param configs        user defined lsm6dsm_t struct (ptr)
 * @return lsm6dsm_err_t error code, LSM6DSM_SUCCESS on success
 */
lsm6dsm_err_t lsm6dsm_check_who_am_i(lsm6dsm_t *const configs);

/**
 * @brief Check if the master device is communicating correctly with the sensor, 
 *        by reading a register, writing it and then reading it and writing it again. 
 * 
 * @param configs          user defined lsm6dsm_t struct (ptr)
 * @return lsm6dsm_err_t   error code, LSM6DSM_SUCCESS on success 
 */
lsm6dsm_err_t lsm6dsm_check_communication(lsm6dsm_t *const configs);

/*! @} */

/*! @defgroup lsm6dsm_communication_functions 
*  lsm6dsm functions
*  @{
*/

/*************** Communication fuctions ***************/

/**
 * @brief Read multiple registers starting from start_reg.
 * 
 * @param configs       user defined lsm6dsm_t struct (ptr)
 * @param data          buffer containing the read data. Must be at least as long as size
 * @param start_reg     address of first register to read, member of @ref lsm6dsm_registers
 * @param size          number of register addresses to read
 * @param update_config switch to enable the storage of the read data in the @ref configs register
 * @return lsm6dsm_err_t 
 */
lsm6dsm_err_t lsm6dsm_read_data(lsm6dsm_t *const configs, uint8_t *const data, uint8_t start_reg, uint8_t size, uint8_t update_config);


/**
  * @brief  Write multiple registers starting from start_reg.
  * 
  * @param configs        user defined lsm6dsm_t struct (ptr)
  * @param data           buffer containing the data to write to the sensor. Must be at least as long as size
  * @param start_reg      address of first register to read, member of @ref lsm6dsm_registers
  * @param size           number of register addresses to read
  * @param update_config  switch to enable the storage of the written data in the @ref configs_register
  * @return lsm6dsm_err_t error code, LSM6DSM_SUCCESS on success
  */
lsm6dsm_err_t lsm6dsm_write_data(lsm6dsm_t *const configs, uint8_t *const data, uint8_t start_reg, uint8_t size, uint8_t update_config);


/**
  * @brief Register read command, read multiple registers from the sensor
  *        and store their value in the config->reg struct.
  *
  * @param configs      user defined lsm6dsm_t struct (ptr)
  * @param start_reg    address of first register to read, member of @ref lsm6dsm_registers 
  * @param size         number of register addresses to read
  * @return lsm6dsm_err_t error code, LSM6DSM_SUCCESS on success
  */
lsm6dsm_err_t lsm6dsm_read_regs(lsm6dsm_t *const configs, uint8_t start_reg, uint8_t size);

/**
 * @brief Register read command, read a register from the sensor
 *        and store its value in the config->reg struct.
 * 
 * @param configs      user defined lsm6dsm_t struct (ptr)
 * @param reg          address of the register to read, member of @ref lsm6dsm_registers 
 * @return lsm6dsm_err_t 
 */
lsm6dsm_err_t lsm6dsm_read_reg(lsm6dsm_t *const configs, uint8_t reg);

/**
 * @brief Register write command, write multiple registers from the config struct to the
 *        sensor memory.
 * 
 * @param configs       user defined lsm6dsm_t struct (ptr)
 * @param start_reg     address of first register to write, member of @ref lsm6dsm_registers 
 * @param size          number of register addresses to read
 * @return lsm6dsm_err_t 
 */
lsm6dsm_err_t lsm6dsm_write_regs(lsm6dsm_t *const configs, uint8_t start_reg, uint8_t size);

/**
 * @brief Register write command, write a register from the config struct to the
 *        sensor memory.
 * 
 * @param configs       user defined lsm6dsm_t struct (ptr)
 * @param reg           address of first register to write, member of @ref lsm6dsm_registers 
 * @return lsm6dsm_err_t 
 */
lsm6dsm_err_t lsm6dsm_write_reg(lsm6dsm_t *const configs, uint8_t reg);

/*! @} */

#ifdef __cplusplus
}
#endif

#endif /* LSM6DSM_H */
