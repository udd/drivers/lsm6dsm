/*
 ******************************************************************************
 * @file    lsm6dsm_default.h
 * @author  Universal Driver Development Team - Federico Villani
 * @brief   lsm6dsm default: this file contains the default settings of the 
 *                            sensor after a reset. The sensor struct can be 
 *                            initialized with LSM6DSM_DEFAULT_REG_VALUES
 *                            to avoid having to read the whole memory. 
 ******************************************************************************
 * @attention
 *
 *  Copyright (c) 2021 UDD Team - Federico Villani. 
 *  All rights reserved.
 *  This work is licensed under the terms of the MIT license.  
 *  For a copy, see <https://opensource.org/licenses/MIT> or
 *  appended LICENSE file.
 *
 ******************************************************************************
 */

#ifndef LSM6DSM_DEFAULT_H
#define LSM6DSM_DEFAULT_H

/*! \addtogroup lsm6dsm_default
*  lsm6dsm default configuration
*  @{
*/
#define LSM6DSM_DEFAULT_REG_VALUES                                    \
  {                                                                   \
    .reg.func_cfg_access.func_cfg_en = LSM6DSM_BANK_A_B_DISABLED,     /**< Disable access to the embedded functions config   **/\
    .reg.sensor_sync_time_frame.tph = 0,                              /**< Sensor synchronization time 500 ms                **/\
    .reg.sensor_sync_res_ratio.res_ratio = LSM6DSM_RES_RATIO_2_11,    /**< Resolution ratio of error code, sensor sync: 2-11 **/\
                                                                      \
    .reg.fifo_ctrl1.fth = 0,                                          \
    .reg.fifo_ctrl2.fth = 0,                                          /**< FIFO threshold level: 0                           **/\
    .reg.fifo_ctrl2.fifo_temp_en = LSM6DSM_DISABLE,                   /**< Disable the temperature data storage in FIFO    	 **/\
    .reg.fifo_ctrl2.timer_pedo_fifo_drdy = LSM6DSM_DISABLE,           /**< Enable write in FIFO based on XL/Gyro data-ready  **/\
    .reg.fifo_ctrl2.timer_pedo_fifo_en = LSM6DSM_DISABLE,             /**< Disable pedometer step counter and timestamp      **/\
    .reg.fifo_ctrl3.dec_fifo_xl = LSM6DSM_NOT_IN_FIFO,                /**< Accelerometer not in FIFO                         **/\
    .reg.fifo_ctrl3.dec_fifo_gyro = LSM6DSM_NOT_IN_FIFO,              /**< Gyroscope not in FIFO                             **/\
    .reg.fifo_ctrl4.dec_ds3_fifo = LSM6DSM_NOT_IN_FIFO,               /**< Third FIFO data set not in FIFO                   **/\
    .reg.fifo_ctrl4.dec_ds4_fifo = LSM6DSM_NOT_IN_FIFO,               /**< Fourth FIFO data set not in FIFO                  **/\
    .reg.fifo_ctrl4.only_high_data = LSM6DSM_DISABLE,                 /**< Disable MSByte memorization in FIFO for XL and GY **/\
    .reg.fifo_ctrl4.stop_on_fth = LSM6DSM_DISABLE,                    /**< Disable FIFO limit on threshold level             **/\
    .reg.fifo_ctrl5.fifo_mode = LSM6DSM_BYPASS_MODE,                  /**< FIFO mode: Bypass mode. FIFO disabled             **/\
    .reg.fifo_ctrl5.odr_fifo = LSM6DSM_FIFO_DISABLE,                  /**< FIFO ODR selection: FIFO disabled                 **/\
                                                                      \
    .reg.drdy_pulse_cfg.int2_wrist_tilt = LSM6DSM_DISABLE,            /**< Disable Wrist tilt interrupt on INT2 pad          **/\
    .reg.drdy_pulse_cfg.drdy_pulsed = LSM6DSM_DISABLE,                /**< Disable pulsed DataReady mode                     **/\
                                                                      \
    .reg.int1_ctrl.drdy_xl = LSM6DSM_DISABLE,                         /**< Disable INT1 pad Accelerometer Data Ready intr    **/\
    .reg.int1_ctrl.drdy_g = LSM6DSM_DISABLE,                          /**< Disable INT1 pad Significant motion intr          **/\
    .reg.int1_ctrl.boot = LSM6DSM_DISABLE,                            /**< Disable INT1 pad FIFO full flag intr              **/\
    .reg.int1_ctrl.fth = LSM6DSM_DISABLE,                             /**< Disable INT1 pad FIFO overrun intr                **/\
    .reg.int1_ctrl.fifo_ovr = LSM6DSM_DISABLE,                        /**< Disable INT1 pad FIFO threshold intr              **/\
    .reg.int1_ctrl.full_flag = LSM6DSM_DISABLE,                       /**< Disable INT1 pad Boot status available intr       **/\
    .reg.int1_ctrl.sign_mot = LSM6DSM_DISABLE,                        /**< Disable INT1 pad Gyroscope Data Ready intr        **/\
    .reg.int1_ctrl.step_detector = LSM6DSM_DISABLE,                   /**< Disable INT1 pad Pedometer step recognition intr  **/\
                                                                      \
    .reg.int2_ctrl.drdy_xl = LSM6DSM_DISABLE,                         /**< Disable INT2 pad Accelerometer Data Ready intr    **/\
    .reg.int2_ctrl.drdy_g = LSM6DSM_DISABLE,                          /**< Disable INT2 pad Gyroscope Data Ready intr        **/\
    .reg.int2_ctrl.drdy_temp = LSM6DSM_DISABLE,                       /**< Disable INT2 pad Temperature Data Ready intr      **/\
    .reg.int2_ctrl.fth = LSM6DSM_DISABLE,                             /**< Disable INT2 pad FIFO threshold intr              **/\
    .reg.int2_ctrl.fifo_ovr = LSM6DSM_DISABLE,                        /**< Disable INT2 pad FIFO overrun intr                **/\
    .reg.int2_ctrl.full_flag = LSM6DSM_DISABLE,                       /**< Disable INT2 pad FIFO full flag intr              **/\
    .reg.int2_ctrl.step_count_ov = LSM6DSM_DISABLE,                   /**< Disable INT2 pad Step counter overflow intr       **/\
    .reg.int2_ctrl.step_delta = LSM6DSM_DISABLE,                      /**< Disable INT2 pad Pedometer step recognition intr  **/\
                                                                      \
    .reg.ctrl1_xl.bw0_xl = LSM6DSM_BW_1_5_KHZ,                        /**< Accelerometer analog chain bandwidth 1500 Hz      **/\
    .reg.ctrl1_xl.lpf1_bw_sel = 0,                                    /**< XL digital low pass filter bandwidth selection. 
                                                                            BW is ODR/2 if: 
                                                                            ctrl8_xl.hp_slope_xl_en = 0, 
                                                                            ctrl8_xl.hp_slope_xl_en = 0, 
                                                                            ctrl8_xl.lpf2_xl_en = 0 **/                         \
    .reg.ctrl1_xl.fs_xl = LSM6DSM_2g,                                 /**< Accelerometer scale min: -2g, max 2g              **/\
    .reg.ctrl1_xl.odr_xl = LSM6DSM_XL_ODR_POWER_DOWN,                 /**< Accelerometer Output data rate: powerdown (XL off)**/\
                                                                      \
    .reg.ctrl2_g.fs_125 = LSM6DSM_DISABLE,                            /**< Gyroscope full-scale at 125 dps disabled          **/\
    .reg.ctrl2_g.fs_g = LSM6DSM_GY_FS_250_DPS,                        /**< Gyroscope full-scale: 250 dps                     **/\
    .reg.ctrl2_g.odr_g = LSM6DSM_GY_ODR_OFF,                          /**< Gyroscope output data rate: power-down (GY off)   **/\
                                                                      \
    .reg.ctrl3_c.sw_reset = 0,                                        /**< No reset, bit clears automatically when set       **/\
    .reg.ctrl3_c.ble = LSM6DSM_LITTLE_ENDIAN_DATA,                    /**< Output data is Little Endian                      **/\
    .reg.ctrl3_c.if_inc = LSM6DSM_ENABLE,                             /**< Register address auto increment for serial comm.  **/\
    .reg.ctrl3_c.sim = LSM6DSM_4_WIRE_SPI,                            /**< 4 wire SPI mode selected                          **/\
    .reg.ctrl3_c.pp_od = LSM6DSM_INT2_PUSH_PULL,                      /**< Push-pull on INT1 and INT2 pads                   **/\
    .reg.ctrl3_c.h_lactive = LSM6DSM_INT_OUT_ACTIVE_HIGH,             /**< Interrupt output pads active high                 **/\
    .reg.ctrl3_c.bdu = LSM6DSM_OUT_REG_UPDATE_CONT,                   /**< Block Data continuous update on output registers  **/\
    .reg.ctrl3_c.boot = 0,                                            /**< Do not reboot the memory content                  **/\
                                                                      \
    .reg.ctrl4_c.lpf1_sel_g = LSM6DSM_DISABLE,                        /**< Disable gyroscope digital low pass filter 1       **/\
    .reg.ctrl4_c.i2c_disable = 0,                                     /**< I2C and SPI interface enabled                     **/\
    .reg.ctrl4_c.drdy_mask = LSM6DSM_DISABLE,                         /**< Data available timer disabled                     **/\
    .reg.ctrl4_c.den_drdy_int1 = LSM6DSM_DISABLE,                     /**< DEN(data enable) DRDY signal on INT1 pad disabled **/\
    .reg.ctrl4_c.int2_on_int1 = LSM6DSM_DISABLE,                      /**< Interrupt signals on both INT1 and INT2 pads      **/\
    .reg.ctrl4_c.sleep = LSM6DSM_DISABLE,                             /**< Gyroscope sleep mode disabled                     **/\
    .reg.ctrl4_c.den_xl_en = LSM6DSM_DISABLE,                         /**< Do not extend DEN functionality to XL sensor.     **/\
                                                                      \
    .reg.ctrl5_c.st_xl = LSM6DSM_XL_SELF_TEST_DISABLE,                /**< Disable linear acceleration sensor self-test      **/\
    .reg.ctrl5_c.st_g = LSM6DSM_GY_SELF_TEST_DISABLE,                 /**< Disable Angular rate sensor self-test             **/\
    .reg.ctrl5_c.den_lh = LSM6DSM_DEN_ACT_LOW,                        /**< DEN active low                                    **/\
    .reg.ctrl5_c.rounding = LSM6DSM_ROUND_DISABLE,                    /**< Disable circular burst mode                       **/\
                                                                      \
    .reg.ctrl6_c.ftype = LSM6DSM_LP_G_BW_LVL0,                        /**< Select Gyroscope's low-pass filter type: 0        **/\
    .reg.ctrl6_c.usr_off_w = LSM6DSM_W_XL_2E_M10,                     /**< Weight of XL user offset registers: 2^-10 g/lsb   **/\
    .reg.ctrl6_c.xl_hm_mode = 0,                                      /**< Enable High-performance mode for accelerometer    **/\
    .reg.ctrl6_c.den_data_level_mode = LSM6DSM_DEN_DISABLE,           /**< Disable DEN data edge-level-sensitive trigger     **/\
                                                                      \
    .reg.ctrl7_g.rounding_status = LSM6DSM_DISABLE,                   /**< Source register rounding disabled                 **/\
    .reg.ctrl7_g.hpm_g = LSM6DSM_HP_16mHz_LP2,                        /**< Gyroscope digital HP filter cutoff at 16mHz       **/\
    .reg.ctrl7_g.hp_en_g = LSM6DSM_DISABLE,                           /**< Disable Gyroscope digital HP filter               **/\
    .reg.ctrl7_g.g_hm_mode = 0,                                       /**< Enable High-performance mode for gyroscope        **/\
                                                                      \
    .reg.ctrl8_xl.low_pass_on_6d = 0,                                 /**< Filter setting: ODR/2 => 6D/4D (Datasheet fig. 9) **/\
    .reg.ctrl8_xl.hp_slope_xl_en = 0,                                 /**< Output of ctrl8_xl.lpf2_xl_en => XL output & FIFO **/\
    .reg.ctrl8_xl.input_composite = 0,                                /**< ODR/2 => LPF2 & HPF                               **/\
    .reg.ctrl8_xl.hp_ref_mode = 0,                                    /**< Disable HP filter reference mode                  **/\
    .reg.ctrl8_xl.hpcf_xl = 0,                                        /**< No effect if ctrl8_xl.lpf2_xl_en = 0   (fig. 9)   **/\
    .reg.ctrl8_xl.lpf2_xl_en = 0,                                     /**< Output of ctrl1_xl.lpf1_bw_sel => hp_slope_xl_en  **/\
                                                                      \
    .reg.ctrl9_xl.soft_en = LSM6DSM_DISABLE,                          /**< Disable magnetometer soft-iron correction         **/\
    .reg.ctrl9_xl.den_xl_g = LSM6DSM_DEN_PIN_GY,                      /**< DEN pin info stamped in GY axis: bit 7..5         **/\
    .reg.ctrl9_xl.den_z = LSM6DSM_ENABLE,                             /**< DEN valued stored in Z-axis LSB                   **/\
    .reg.ctrl9_xl.den_y = LSM6DSM_ENABLE,                             /**< DEN valued stored in Y-axis LSB                   **/\
    .reg.ctrl9_xl.den_x = LSM6DSM_ENABLE,                             /**< DEN valued stored in X-axis LSB                   **/\
                                                                      \
    .reg.ctrl10_c.sign_motion_en = LSM6DSM_DISABLE,                   /**< Disable significant motion detection function     **/\
    .reg.ctrl10_c.pedo_rst_step = LSM6DSM_DISABLE,                    /**< Do not Reset pedometer step counter.              **/\
    .reg.ctrl10_c.func_en = LSM6DSM_DISABLE,                          /**< Disable embedded functionalities (some below)     **/\
    .reg.ctrl10_c.tilt_en = LSM6DSM_DISABLE,                          /**< Disable Tilt calculation                          **/\
    .reg.ctrl10_c.pedo_en = LSM6DSM_DISABLE,                          /**< Disable pedometer algorithm                       **/\
    .reg.ctrl10_c.timer_en = LSM6DSM_DISABLE,                         /**< Disable Timestamp count                           **/\
    .reg.ctrl10_c.wrist_tilt_en = LSM6DSM_DISABLE,                    /**< Disable wrist tilt algorithm                      **/\
                                                                      \
    .reg.master_config.master_on = LSM6DSM_DISABLE,                   /**< Disable master I2C of sensor hub                  **/\
    .reg.master_config.iron_en = LSM6DSM_DISABLE,                     /**< Disable hard-iron correction  magnetometer        **/\
    .reg.master_config.pass_through_mode = LSM6DSM_DISABLE,           /**< Disable I2C interface pass-through                **/\
    .reg.master_config.pull_up_en = LSM6DSM_DISABLE,                  /**< Disable internal pull-up on Auxiliary I2C line    **/\
    .reg.master_config.start_config = LSM6DSM_XL_GY_DRDY,             /**< Sensor hub trigger signal is the XL/GY data-ready **/\
    .reg.master_config.data_valid_sel_fifo = LSM6DSM_DV_XL_GY_ST,     /**< Data-valid signal to write FIFO: data ready XL/GY **/\
    .reg.master_config.drdy_on_int1 = LSM6DSM_DISABLE,                /**< Disable Master DRDY signal on INT1                **/\
                                                                      \
    .reg.tap_cfg.lir = LSM6DSM_DISABLE,                               /**< Interrupt request not latched                     **/\
    .reg.tap_cfg.tap_z_en = LSM6DSM_DISABLE,                          /**< Disable Z direction in tap recognition            **/\
    .reg.tap_cfg.tap_y_en = LSM6DSM_DISABLE,                          /**< Disable Y direction in tap recognition            **/\
    .reg.tap_cfg.tap_x_en = LSM6DSM_DISABLE,                          /**< Disable X direction in tap recognition            **/\
    .reg.tap_cfg.slope_fds = LSM6DSM_FILTER_SLOPE,                    /**< SLOPE filter applied on wake up and act/inact func**/\
    .reg.tap_cfg.inact_en = LSM6DSM_PROPERTY_DISABLE,                 /**< Disable inactivity function                       **/\
    .reg.tap_cfg.interrupts_enable = LSM6DSM_DISABLE,                 /**< Disable basic interrupts                          **/\
                                                                      \
    .reg.tap_ths_6d.tap_ths = 0,                                      /**< Threshold for tap recognition: 0                  **/\
    .reg.tap_ths_6d.sixd_ths = 0,                                     /**< Threshold for 4D/6D function: 80 deg              **/\
    .reg.tap_ths_6d.d4d_disable = 0,                                  /**< Enable 4D orientation detection                   **/\
                                                                      \
    .reg.int_dur2.shock = 0,                                          /**< Max duration of overthreshold event: 4*ODR_XL     **/\
    .reg.int_dur2.quiet = 0,                                          /**< Quiet time after a tap detection: 2*ODR_XL        **/\
    .reg.int_dur2.dur = 0,                                            /**< Max time gap  double tap recognition: 16*ODR_XL   **/\
                                                                      \
    .reg.wake_up_ths.wk_ths = 0,                                      /**< Threshold for wakeup: 0                           **/\
    .reg.wake_up_ths.single_double_tap = LSM6DSM_DISABLE,             /**< Enable only single-tap event                      **/\
                                                                      \
    .reg.wake_up_dur.sleep_dur = 0,                                   /**< Duration to go in sleep mode: 16*ODR              **/\
    .reg.wake_up_dur.timer_hr = LSM6DSM_LSB_6ms4,                     /**< Timestamp register resolution: 1 LSB = 6.4 ms     **/\
    .reg.wake_up_dur.wake_dur = 0,                                    /**< Wake up duration event: 0                         **/\
    .reg.wake_up_dur.ff_dur = 0,                                      /**< Free fall duration event MSb: 0                   **/\
                                                                      \
    .reg.free_fall.ff_ths = LSM6DSM_FF_TSH_156mg,                     /**< Free fall threshold: 156 mg                       **/\
    .reg.free_fall.ff_dur = 0,                                        /**< Free-fall duration event 4 LSb: 0                 **/\
                                                                      \
    .reg.md1_cfg.int1_timer = LSM6DSM_DISABLE,                        /**< Disable routing of timer end counter event, INT1  **/\
    .reg.md1_cfg.int1_tilt = LSM6DSM_DISABLE,                         /**< Disable routing of tilt event on INT1             **/\
    .reg.md1_cfg.int1_6d = LSM6DSM_DISABLE,                           /**< Disable routing of 6D event on INT1               **/\
    .reg.md1_cfg.int1_double_tap = LSM6DSM_DISABLE,                   /**< Disable routing of tap event on INT1              **/\
    .reg.md1_cfg.int1_ff = LSM6DSM_DISABLE,                           /**< Disable routing of free-fall event on INT1        **/\
    .reg.md1_cfg.int1_wu = LSM6DSM_DISABLE,                           /**< Disable routing of wakeup event on INT1           **/\
    .reg.md1_cfg.int1_single_tap = LSM6DSM_DISABLE,                   /**< Disable routing of Single-tap recognition on INT1 **/\
    .reg.md1_cfg.int1_inact_state = LSM6DSM_DISABLE,                  /**< Disable routing of inactivity mode on INT1        **/\
                                                                      \
    .reg.md2_cfg.int2_iron = LSM6DSM_DISABLE,                         /**< Disable routing of soft-/hard-iron end event INT2 **/\
    .reg.md2_cfg.int2_tilt = LSM6DSM_DISABLE,                         /**< Disable routing of tilt event on INT2             **/\
    .reg.md2_cfg.int2_6d = LSM6DSM_DISABLE,                           /**< Disable routing of 6D event on INT2               **/\
    .reg.md2_cfg.int2_double_tap = LSM6DSM_DISABLE,                   /**< Disable routing of tap event on INT2              **/\
    .reg.md2_cfg.int2_ff = LSM6DSM_DISABLE,                           /**< Disable routing of free-fall event on INT2        **/\
    .reg.md2_cfg.int2_wu = LSM6DSM_DISABLE,                           /**< Disable routing of wakeup event on INT2           **/\
    .reg.md2_cfg.int2_single_tap = LSM6DSM_DISABLE,                   /**< Disable routing of Single-tap recognition on INT2 **/\
    .reg.md2_cfg.int2_inact_state = LSM6DSM_DISABLE,                  /**< Disable routing of inactivity mode on INT2        **/\
                                                                      \
    .reg.master_cmd_code = 0,                                         /**< Master command code for sensor sync stamping: 0   **/\
    .reg.sens_sync_spi_errc = 0,                                      /**< Error code used for sensor synchronization: 0     **/\
                                                                      \
    .reg.int_ois.lvl2_ois = LSM6DSM_DISABLE,                          /**< Disable level-sensitive latched mode on OIS       **/\
    .reg.int_ois.int2_drdy_ois = LSM6DSM_DISABLE,                     /**< Disable the OIS chain DRDY on the INT2   	       **/\
                                                                      \
    .reg.ctrl1_ois.ois_en_spi2 = LSM6DSM_DISABLE,                     /**< Disable OIS chain data processing gyro. Mode 3, 4 **/\
    .reg.ctrl1_ois.fs_g_ois = LSM6DSM_250dps_AUX,                     /**< Gyroscope OIS chain full-scale: 250 dps           **/\
    .reg.ctrl1_ois.mode4_en = LSM6DSM_DISABLE,                        /**< Disable accelerometer OIS chain                   **/\
    .reg.ctrl1_ois.sim_ois = LSM6DSM_4_WIRE_SPI,                      /**< Select 4-wire SPI2                                **/\
    .reg.ctrl1_ois.lvl1_ois = LSM6DSM_DISABLE,                        /**< Disable level-sensitive trigger mode on OIS chain **/\
    .reg.ctrl1_ois.ble_ois = LSM6DSM_LITTLE_ENDIAN_DATA,              /**< LSbyte at lower register address                  **/\
                                                                      \
    .reg.ctrl2_ois.hp_en_ois = LSM6DSM_DISABLE,                       /**< Disable gyroscope's OIS chain HPF                 **/\
    .reg.ctrl2_ois.ftype_ois = LSM6DSM_BW_351_Hz,                     /**< Gyroscope's digital LPF1 filter bandwidth: 351 Hz **/\
    .reg.ctrl2_ois.hpm_ois = LSM6DSM_HP_16_mHz,                       /**< Gyro OIS digital high-pass filter cutoff: 16 mHz  **/\
                                                                      \
    .reg.ctrl3_ois.st_ois_clampdis = LSM6DSM_DISABLE,                 /**< All gyro OIS chain outputs: 8000h in Self test    **/\
    .reg.ctrl3_ois.st_ois = 0,                                        /**< Gyroscope OIS chain self-test: Normal mode        **/\
    .reg.ctrl3_ois.filter_xl_conf_ois = 0,                            /**< Accelerometer OIS channel BW: 128 Hz (ODR default)**/\
    .reg.ctrl3_ois.fs_xl_ois = 0,                                     /**< Accelerometer OIS channel full-scale: 2 g         **/\
    .reg.ctrl3_ois.den_lh_ois = 0,                                    /**< Active-low DEN signal pin on OIS chain            **/\
                                                                      \
    .reg.x_ofs_usr = 0,                                               /**< Accelerometer X-axis user offset correction: 0    **/\
    .reg.y_ofs_usr = 0,                                               /**< Accelerometer Y-axis user offset correction: 0    **/\
    .reg.z_ofs_usr = 0,                                               /**< Accelerometer Z-axis user offset correction: 0    **/\
     \
  }

/*! @} */

#endif /* LSM6DSM_DEFAULT_H */
