/*
 ******************************************************************************
 * @file    lsm6dsm_ex.c
 * @author  Universal Driver Development Team - villanif
 * @brief   lsm6dsm expansion functions: This file contains expansion function 
 *              that help with reading, conversion of measurements and 
 *              advanced configurations
 ******************************************************************************
 * @attention
 *
 *  Copyright (c) 2021 UDD Team - Federico Villani. 
 *  All rights reserved.
 *  This work is licensed under the terms of the MIT license.  
 *  For a copy, see <https://opensource.org/licenses/MIT> or
 *  appended LICENSE file.
 *
 ******************************************************************************
 */
#include "lsm6dsm_ex.h"


lsm6dsm_err_t lsm6dsm_calc_accel_data_mG(lsm6dsm_t *const configs, float *xyz)
{
    uint32_t accel_mG_fs = 0;

    switch (configs->reg.ctrl1_xl.fs_xl)
    {
    case LSM6DSM_2g:
        accel_mG_fs = 2000;
        break;
    case LSM6DSM_4g:
        accel_mG_fs = 4000;
        break;
    case LSM6DSM_8g:
        accel_mG_fs = 8000;
        break;
    case LSM6DSM_16g:
        accel_mG_fs = 16000;
        break;
    default:
        return LSM6DSM_ERR_CONFIG;
    }
    int16_t *xyz_raw = (int16_t *)&configs->reg.outx_xl;
    xyz[0] = (float)xyz_raw[0] * accel_mG_fs / (1 << 15);
    xyz[1] = (float)xyz_raw[1] * accel_mG_fs / (1 << 15);
    xyz[2] = (float)xyz_raw[2] * accel_mG_fs / (1 << 15);
    return LSM6DSM_SUCCESS;
}

lsm6dsm_err_t lsm6dsm_calc_gyro_data_dps(lsm6dsm_t *const configs, float *xyz)
{
    uint32_t gyro_dps_fs = 0;

    switch (configs->reg.ctrl2_g.fs_g)
    {
    case LSM6DSM_GY_FS_250_DPS:
        gyro_dps_fs = 250;
        break;
    case LSM6DSM_GY_FS_500_DPS:
        gyro_dps_fs = 500;
        break;
    case LSM6DSM_GY_FS_1000_DPS:
        gyro_dps_fs = 1000;
        break;
    case LSM6DSM_GY_FS_2000_DPS:
        gyro_dps_fs = 2000;
        break;
    default:
        return LSM6DSM_ERR_CONFIG;
    }
    if (configs->reg.ctrl2_g.fs_125)
        gyro_dps_fs = 125;

    int16_t* xyz_raw = (int16_t*)&configs->reg.outx_gy;
    xyz[0] = (float)xyz_raw[0] * gyro_dps_fs / (1 << 15);
    xyz[1] = (float)xyz_raw[1] * gyro_dps_fs / (1 << 15);
    xyz[2] = (float)xyz_raw[2] * gyro_dps_fs / (1 << 15);
    return LSM6DSM_SUCCESS;
}

lsm6dsm_err_t lsm6dsm_get_accel_data(lsm6dsm_t *const configs, float *xyz)
{
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;
    err |= lsm6dsm_read_reg(configs, LSM6DSM_STATUS_REG);
    if (configs->reg.status_reg_status_spi.xlda)
    {
        err |= lsm6dsm_read_regs(configs, LSM6DSM_OUTX_L_XL, 6);
        err |= lsm6dsm_calc_accel_data_mG(configs, xyz);
        return err;
    }
    return LSM6DSM_NO_NEW_DATA;
}

lsm6dsm_err_t lsm6dsm_get_gyro_data(lsm6dsm_t *const configs, float *xyz)
{
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;
    err |= lsm6dsm_read_reg(configs, LSM6DSM_STATUS_REG);
    if (configs->reg.status_reg_status_spi.gda)
    {
        err |= lsm6dsm_read_regs(configs, LSM6DSM_OUTX_L_G, 6);
        err |= lsm6dsm_calc_gyro_data_dps(configs, xyz);
        return err;
    }
    return LSM6DSM_NO_NEW_DATA;
}