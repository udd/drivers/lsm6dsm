/*
 ******************************************************************************
 * @file    lsm6dsm.c
 * @author  Universal Driver Development Team - villanif
 * @brief   lsm6dsm driver file
 ******************************************************************************
 * @attention
 *
 *  Copyright (c) 2021 UDD Team - Federico Villani. 
 *  All rights reserved.
 *  This work is licensed under the terms of the MIT license.  
 *  For a copy, see <https://opensource.org/licenses/MIT> or
 *  appended LICENSE file.
 *
 ******************************************************************************
 */

/*************** Includes ***************/
#include "lsm6dsm.h"


/*************** Static variables ***************/
static lsm6dsm_communication_t *lsm6dsm_comm;
static uint32_t lsm6dsm_writable_regs[4] = {LSM6DSM_00_1F_WRITE_BITMASK,
                                            LSM6DSM_20_3F_WRITE_BITMASK,
                                            LSM6DSM_40_5F_WRITE_BITMASK,
                                            LSM6DSM_60_7F_WRITE_BITMASK};
static uint32_t lsm6dsm_readable_regs[4] = {LSM6DSM_00_1F_READ_BITMASK,
                                            LSM6DSM_20_3F_READ_BITMASK,
                                            LSM6DSM_40_5F_READ_BITMASK,
                                            LSM6DSM_60_7F_READ_BITMASK};

/*************** Static fuctions ***************/
static inline lsm6dsm_err_t check_struct_pointer(lsm6dsm_t *const configs)
{
    return configs == 0 ||
           (configs->comm.write) == 0 ||
           (configs->comm.read) == 0 ||
           &(configs->reg) == 0;
}

static inline lsm6dsm_err_t lsm_memcmp(const void *mem1, const void *mem2, uint32_t len)
{
    if (LSM6DSM_NULL_PNT(mem1) || LSM6DSM_NULL_PNT(mem2))
        return LSM6DSM_ERROR;
    uint8_t *m1 = (uint8_t *)mem1;
    uint8_t *m2 = (uint8_t *)mem2;
    if (m1 == m2 || !len)
        return 0;
    for (; 0 < len; --len, ++m1, ++m2)
    {
        if (*m1 != *m2)
            return LSM6DSM_ERR_VALUE;
    }
    return LSM6DSM_SUCCESS;
}
static inline lsm6dsm_err_t lsm_memcpy(void *const dst, const void *src, uint32_t len)
{
    if (LSM6DSM_NULL_PNT(dst) || LSM6DSM_NULL_PNT(src))
        return LSM6DSM_ERROR;
    uint8_t *d = (uint8_t *)dst;
    uint8_t *s = (uint8_t *)src;
    if (d == s || !len)
        return LSM6DSM_SUCCESS;
    for (; 0 < len; --len, ++d, ++s)
    {
        *d = *s;
    }
    return LSM6DSM_SUCCESS;
}

static inline uint8_t lsm6dsm_reg_get_writable_block(uint8_t start_reg)
{
    uint8_t size = 0;
    while (start_reg < LSM6DSM_REG_SIZE)
    {
        /* Checks if the bit corresponding to the register address is not set */
        if (!(lsm6dsm_writable_regs[start_reg / 32] & (1 << (start_reg % 32))))
            return size;
        ++size;
        ++start_reg;
    }
    return size;
}

static lsm6dsm_err_t lsm6dsm_reg_check_writable(uint8_t start_reg, uint8_t size)
{
    if (size + start_reg > LSM6DSM_REG_SIZE)
        return LSM6DSM_ERROR;
    while (size)
    {
        /* Checks if the bit corresponding to the register address is not set */
        if (!(lsm6dsm_writable_regs[start_reg / 32] & (1 << (start_reg % 32))))
            return LSM6DSM_ERROR;
        --size;
    }
    return LSM6DSM_SUCCESS;
}

static lsm6dsm_err_t lsm6dsm_reg_check_readable(uint8_t start_reg, uint8_t size)
{
    if (size + start_reg > LSM6DSM_REG_SIZE)
        return LSM6DSM_ERROR;
    while (size)
    {
        /* Checks if the bit corresponding to the register address is not set */
        if (!(lsm6dsm_readable_regs[start_reg / 32] & (1 << (start_reg % 32))))
            return LSM6DSM_ERROR;
        --size;
        ++start_reg;
    }
    return LSM6DSM_SUCCESS;
}

static lsm6dsm_err_t lsm6dsm_update_conf_reg(lsm6dsm_t *const configs, uint8_t *src, uint8_t start_reg, uint8_t size)
{
    if ((lsm6dsm_reg_check_writable(start_reg, size) &&
         lsm6dsm_reg_check_readable(start_reg, size)) ||
        check_struct_pointer(configs))
        return LSM6DSM_ERROR;
    return lsm_memcpy(((void *)&configs->reg) + start_reg, src, size);
}

/*************** Setup fuctions ***************/
lsm6dsm_err_t lsm6dsm_setup_communication(lsm6dsm_t *const configs, lsm6dsm_sim_t spi_mode)
{
    /* check size of lsm6dsm register struct at compile time  */
    LSM_CHECK_REG_SIZE_COMPILE_TIME(lsm6dsm_registers_t, 0x80);

    if (check_struct_pointer(configs))
        return LSM6DSM_ERR_CONFIG;

    /* Save pointer to function locally (needed for forward compatibility of interrupts) */
    lsm6dsm_comm = &(configs->comm);

    lsm6dsm_err_t err = LSM6DSM_SUCCESS;

    /* set spi mode and apply modifications */
    configs->reg.ctrl3_c.sim = spi_mode;
    err |= lsm6dsm_write_reg(configs, LSM6DSM_CTRL3_C);
    err |= lsm6dsm_check_communication(configs);
    return err;
}

lsm6dsm_err_t lsm6dsm_setup(lsm6dsm_t *const configs)
{
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;

    if (check_struct_pointer(configs))
        return LSM6DSM_ERR_CONFIG;

    uint8_t register_add = 0;
    while (register_add < LSM6DSM_REG_SIZE)
    {
        uint8_t writable_block = lsm6dsm_reg_get_writable_block(register_add);
        err |= lsm6dsm_write_regs(configs, register_add, writable_block);
        register_add += writable_block + 1;
    }
    return err;
}

lsm6dsm_err_t lsm6dsm_setup_check(lsm6dsm_t *const configs)
{
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;
    lsm6dsm_registers_t buff_regs;
    if (check_struct_pointer(configs))
        return LSM6DSM_ERR_CONFIG;

    uint8_t register_add = 0;
    while (register_add < LSM6DSM_REG_SIZE)
    {
        uint8_t writable_block = lsm6dsm_reg_get_writable_block(register_add);
        err |= lsm6dsm_read_data(configs, LSM6DSM_SCT_TO_P(buff_regs), register_add, writable_block, 0);
        register_add += writable_block + 1;
    }
    err |= lsm_memcmp(LSM6DSM_SCT_TO_P(configs->reg), LSM6DSM_SCT_TO_P(buff_regs), LSM6DSM_REG_SIZE - 1);
    return err;
}

lsm6dsm_err_t lsm6dsm_check_who_am_i(lsm6dsm_t *const configs)
{
    if (check_struct_pointer(configs))
        return LSM6DSM_ERR_CONFIG;
    uint8_t who_am_i_response;
    lsm6dsm_read_data(configs, &who_am_i_response, LSM6DSM_WHO_AM_I, 1, 0);
    if (LSM6DSM_WHO_AM_I_VAL == who_am_i_response)
        return LSM6DSM_SUCCESS;
    else
        return LSM6DSM_ERR_RESPONSE;
}

lsm6dsm_err_t lsm6dsm_check_communication(lsm6dsm_t *const configs)
{
    uint8_t buffer[2];
    uint8_t cmp[2];
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;

    lsm6dsm_fifo_ctrl1_t ctrl1_test = {.fth = 0xAA};
    lsm6dsm_fifo_ctrl2_t ctrl2_test = {
        .fth = 3,
        .timer_pedo_fifo_drdy = 1,
    };
    buffer[0] = LSM6DSM_SCT_TO_B(ctrl1_test);
    buffer[1] = LSM6DSM_SCT_TO_B(ctrl2_test);

    /* Check correct reading of registers, then read, write and read back */
    if (lsm6dsm_check_who_am_i(configs))
        return LSM6DSM_ERR_RESPONSE;

    err |= lsm6dsm_read_regs(configs, LSM6DSM_FIFO_CTRL1, 2);
    err |= lsm6dsm_write_data(configs, buffer, LSM6DSM_FIFO_CTRL1, 2, 0);
    err |= lsm6dsm_read_data(configs, cmp, LSM6DSM_FIFO_CTRL1, 2, 0);
    err |= lsm_memcmp(buffer, cmp, 2);
    err |= lsm6dsm_write_regs(configs, LSM6DSM_FIFO_CTRL1, 2);

    return err;
}

/*************** Communication fuctions ***************/
lsm6dsm_err_t lsm6dsm_read_data(lsm6dsm_t *const configs, uint8_t *data, uint8_t start_reg, uint8_t size, uint8_t upd_config)
{
    if (check_struct_pointer(configs) || lsm6dsm_reg_check_readable(start_reg, size))
        return LSM6DSM_ERR_CONFIG;
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;

    configs->comm.read(start_reg, data, size);
    if (upd_config)
        err |= lsm6dsm_update_conf_reg(configs, data, start_reg, size);
    return err;
}

lsm6dsm_err_t lsm6dsm_write_data(lsm6dsm_t *const configs, uint8_t *data, uint8_t start_reg, uint8_t size, uint8_t update_config)
{
    if (check_struct_pointer(configs) || lsm6dsm_reg_check_writable(start_reg, size))
        return LSM6DSM_ERR_CONFIG;
    lsm6dsm_err_t err = LSM6DSM_SUCCESS;

    configs->comm.write(start_reg, data, size);
    if (update_config)
        err |= lsm6dsm_update_conf_reg(configs, data, start_reg, size);
    return err;
}

lsm6dsm_err_t lsm6dsm_read_regs(lsm6dsm_t *const configs, uint8_t start_reg, uint8_t size)
{
    return lsm6dsm_read_data(configs, LSM6DSM_SCT_REG_ADD(configs, start_reg), start_reg, size, 0);
}

lsm6dsm_err_t lsm6dsm_write_regs(lsm6dsm_t *const configs, uint8_t start_reg, uint8_t size)
{
    //since data is already in configs, do not care about writing it in
    return lsm6dsm_write_data(configs, LSM6DSM_SCT_REG_ADD(configs, start_reg), start_reg, size, 0);
}

lsm6dsm_err_t lsm6dsm_write_reg(lsm6dsm_t *const configs, uint8_t reg)
{
    return lsm6dsm_write_regs(configs, reg, 1);
}

lsm6dsm_err_t lsm6dsm_read_reg(lsm6dsm_t *configs, uint8_t reg)
{
    return lsm6dsm_read_regs(configs, reg, 1);
}
