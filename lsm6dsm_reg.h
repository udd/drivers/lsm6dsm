/*
 ******************************************************************************
 * @file    lsm6dsm_reg.h
 * @author  Universal Driver Development Team - Federico Villani
 * @brief   lsm6dsm registers: this file contains the address of the registers,
 *                             enums for register settings and bitfield structs
 *                             for the setting registers 
 ******************************************************************************
 * @attention
 *
 *  Copyright (c) 2021 UDD Team - Federico Villani. 
 *  All rights reserved.
 *  This work is licensed under the terms of the MIT license.  
 *  For a copy, see <https://opensource.org/licenses/MIT> or
 *  appended LICENSE file.
 *
 ******************************************************************************
 */

#ifndef LSM6DSM_REG_H
#define LSM6DSM_REG_H

/*************** INCLUDE ***************/
#include <stdint.h> 



/*! @defgroup lsm6dsm_registers Group: registers    
*  lsm6dsm registers
*  @{
*/

/*********** REGISTERS ***********/

/* Setup registers */
#define LSM6DSM_FUNC_CFG_ACCESS          (uint8_t)0x01U  /**< Enable embedded functions register (r/w) **/
#define LSM6DSM_SENSOR_SYNC_TIME_FRAME   (uint8_t)0x04U  /**< Sensor synchronization time frame register (r/w) **/
#define LSM6DSM_SENSOR_SYNC_RES_RATIO    (uint8_t)0x05U  /**< Sensor synchronization resolution ratio (r/w) **/

#define LSM6DSM_FIFO_CTRL1               (uint8_t)0x06U  /**< FIFO control register 1 (r/w) **/
#define LSM6DSM_FIFO_CTRL2               (uint8_t)0x07U  /**< FIFO control register 2 (r/w) **/
#define LSM6DSM_FIFO_CTRL3               (uint8_t)0x08U  /**< FIFO control register 3 (r/w) **/
#define LSM6DSM_FIFO_CTRL4               (uint8_t)0x09U  /**< FIFO control register 4 (r/w) **/
#define LSM6DSM_FIFO_CTRL5               (uint8_t)0x0AU  /**< FIFO control register 5 (r/w) **/

#define LSM6DSM_DRDY_PULSE_CFG_G         (uint8_t)0x0BU  /**< DataReady configuration register (r/w)**/
#define LSM6DSM_INT1_CTRL                (uint8_t)0x0DU  /**< INT1 pad control register (r/w) **/
#define LSM6DSM_INT2_CTRL                (uint8_t)0x0EU  /**< INT2 pad control register (r/w) **/

#define LSM6DSM_WHO_AM_I                 (uint8_t)0x0FU  /**< Who_AM_I register (r), value fixed at 6Ah **/

#define LSM6DSM_CTRL1_XL                 (uint8_t)0x10U  /**< Linear acceleration sensor control register 1 (r/w) **/
#define LSM6DSM_CTRL2_G                  (uint8_t)0x11U  /**< Angular rate sensor control register 2 (r/w) **/
#define LSM6DSM_CTRL3_C                  (uint8_t)0x12U  /**< Control register 3 (r/w) **/
#define LSM6DSM_CTRL4_C                  (uint8_t)0x13U  /**< Control register 4 (r/w) **/
#define LSM6DSM_CTRL5_C                  (uint8_t)0x14U  /**< Control register 5 (r/w) **/
#define LSM6DSM_CTRL6_C                  (uint8_t)0x15U  /**< Angular rate sensor control register 6 (r/w) **/
#define LSM6DSM_CTRL7_G                  (uint8_t)0x16U  /**< Angular rate sensor control register 7 (r/w) **/
#define LSM6DSM_CTRL8_XL                 (uint8_t)0x17U  /**< Linear acceleration sensor control register 8 (r/w) **/
#define LSM6DSM_CTRL9_XL                 (uint8_t)0x18U  /**< Linear acceleration sensor control register 9 (r/w) **/
#define LSM6DSM_CTRL10_C                 (uint8_t)0x19U  /**< Control register 10 (r/w) **/

#define LSM6DSM_MASTER_CONFIG            (uint8_t)0x1AU  /**< Master configuration register (r/w) **/
#define LSM6DSM_WAKE_UP_SRC              (uint8_t)0x1BU  /**< Wake up interrupt source register (r) **/
#define LSM6DSM_TAP_SRC                  (uint8_t)0x1CU  /**< Tap source register (r) **/
#define LSM6DSM_D6D_SRC                  (uint8_t)0x1DU  /**< Portrait, landscape, face-up and face-down source register (r) **/
#define LSM6DSM_STATUS_REG               (uint8_t)0x1EU  /**< Status register (r) **/


#define LSM6DSM_OUT_TEMP_L               (uint8_t)0x20U  /**< Temperature data output register Bit 7..0 (r) **/
#define LSM6DSM_OUT_TEMP_H               (uint8_t)0x21U  /**< Temperature data output register Bit 15..8 (r) **/


#define LSM6DSM_OUTX_L_G                 (uint8_t)0x22U  /**< Angular rate pitch axis X Bit 7..0 (r)**/
#define LSM6DSM_OUTX_H_G                 (uint8_t)0x23U  /**< Angular rate pitch axis X Bit 15..8 (r)**/
#define LSM6DSM_OUTY_L_G                 (uint8_t)0x24U  /**< Angular rate pitch axis Y Bit 7..0 (r)**/
#define LSM6DSM_OUTY_H_G                 (uint8_t)0x25U  /**< Angular rate pitch axis Y Bit 15..8 (r)**/
#define LSM6DSM_OUTZ_L_G                 (uint8_t)0x26U  /**< Angular rate pitch axis Z Bit 7..0 (r)**/
#define LSM6DSM_OUTZ_H_G                 (uint8_t)0x27U  /**< Angular rate pitch axis Z Bit 15..8 (r)**/

#define LSM6DSM_OUTX_L_XL                (uint8_t)0x28U  /**< Linear acceleration axis X Bit 7..0 (r)**/
#define LSM6DSM_OUTX_H_XL                (uint8_t)0x29U  /**< Linear acceleration axis X Bit 15..8 (r)**/
#define LSM6DSM_OUTY_L_XL                (uint8_t)0x2AU  /**< Linear acceleration axis Y Bit 7..0 (r)**/
#define LSM6DSM_OUTY_H_XL                (uint8_t)0x2BU  /**< Linear acceleration axis Y Bit 15..8 (r)**/
#define LSM6DSM_OUTZ_L_XL                (uint8_t)0x2CU  /**< Linear acceleration axis Z Bit 7..0 (r)**/
#define LSM6DSM_OUTZ_H_XL                (uint8_t)0x2DU  /**< Linear acceleration axis Z Bit 15..8 (r)**/

/** refer to @ref lsm6dsm_embedded_registers for following sensorhub bytes functionality **/
#define LSM6DSM_SENSORHUB1_REG           (uint8_t)0x2EU  /**< byte 1 associated to external sensors **/
#define LSM6DSM_SENSORHUB2_REG           (uint8_t)0x2FU  /**< byte 2 associated to external sensors **/
#define LSM6DSM_SENSORHUB3_REG           (uint8_t)0x30U  /**< byte 3 associated to external sensors **/
#define LSM6DSM_SENSORHUB4_REG           (uint8_t)0x31U  /**< byte 4 associated to external sensors **/
#define LSM6DSM_SENSORHUB5_REG           (uint8_t)0x32U  /**< byte 5 associated to external sensors **/
#define LSM6DSM_SENSORHUB6_REG           (uint8_t)0x33U  /**< byte 6 associated to external sensors **/
#define LSM6DSM_SENSORHUB7_REG           (uint8_t)0x34U  /**< byte 7 associated to external sensors **/
#define LSM6DSM_SENSORHUB8_REG           (uint8_t)0x35U  /**< byte 8 associated to external sensors **/
#define LSM6DSM_SENSORHUB9_REG           (uint8_t)0x36U  /**< byte 9 associated to external sensors **/
#define LSM6DSM_SENSORHUB10_REG          (uint8_t)0x37U  /**< byte 10 associated to external sensors **/
#define LSM6DSM_SENSORHUB11_REG          (uint8_t)0x38U  /**< byte 11 associated to external sensors **/ 
#define LSM6DSM_SENSORHUB12_REG          (uint8_t)0x39U  /**< byte 12 associated to external sensors **/

/** For a proper reading of the following registers, 
 *  it is recommended to set the BDU bit in CTRL3_C (12h) to 1 **/
#define LSM6DSM_FIFO_STATUS1             (uint8_t)0x3AU  /**< FIFO status control 1 (r) **/
#define LSM6DSM_FIFO_STATUS2             (uint8_t)0x3BU  /**< FIFO status control 2 (r)**/
#define LSM6DSM_FIFO_STATUS3             (uint8_t)0x3CU  /**< FIFO status control 3 (r)**/
#define LSM6DSM_FIFO_STATUS4             (uint8_t)0x3DU  /**< FIFO status control 4 (r) **/

#define LSM6DSM_FIFO_DATA_OUT_L          (uint8_t)0x3EU  /**< FIFO data output - Bit 7..0 (r)**/
#define LSM6DSM_FIFO_DATA_OUT_H          (uint8_t)0x3FU  /**< FIFO data output Bit - 15..8 (r)**/

/** to reset the Timestamp timer, (uint8_t)0xAA has to be written in TIMESTAMP2_REG **/
#define LSM6DSM_TIMESTAMP0_REG           (uint8_t)0x40U  /**< Timestamp data output - Bit 7..0 (r) **/
#define LSM6DSM_TIMESTAMP1_REG           (uint8_t)0x41U  /**< Timestamp data output - Bit 15..8 (r) **/
#define LSM6DSM_TIMESTAMP2_REG           (uint8_t)0x42U  /**< Timestamp data output - Bit 23..16 (r/w) **/
#define LSM6DSM_STEP_TIMESTAMP_L         (uint8_t)0x49U  /**< Step counter timestamp - contains TIMESTAMP0 (r)**/
#define LSM6DSM_STEP_TIMESTAMP_H         (uint8_t)0x4AU  /**< Step counter timestamp - contains TIMESTAMP1 (r)**/
#define LSM6DSM_STEP_COUNTER_L           (uint8_t)0x4BU  /**< Step counter output register - Bit 7..0 (r) **/
#define LSM6DSM_STEP_COUNTER_H           (uint8_t)0x4CU  /**< Step counter output register - Bit 13..8 (r) **/

#define LSM6DSM_SENSORHUB13_REG          (uint8_t)0x4DU  /**< byte 13 associated to external sensors **/
#define LSM6DSM_SENSORHUB14_REG          (uint8_t)0x4EU  /**< byte 14 associated to external sensors **/
#define LSM6DSM_SENSORHUB15_REG          (uint8_t)0x4FU  /**< byte 15 associated to external sensors **/
#define LSM6DSM_SENSORHUB16_REG          (uint8_t)0x50U  /**< byte 16 associated to external sensors **/
#define LSM6DSM_SENSORHUB17_REG          (uint8_t)0x51U  /**< byte 17 associated to external sensors **/
#define LSM6DSM_SENSORHUB18_REG          (uint8_t)0x52U  /**< byte 18 associated to external sensors **/
  
#define LSM6DSM_FUNC_SRC1                (uint8_t)0x53U  /**< Significant motion, tilt, step detector,   **/ 
                                                    /** hard/soft-iron and sensor hub interrupt source (r) **/
#define LSM6DSM_FUNC_SRC2                (uint8_t)0x54U  /**< Wrist tilt and slave NAK interrupt source (r) **/

#define LSM6DSM_WRIST_TILT_IA            (uint8_t)0x55U  /**< Wrist tilt interrupt source (r) **/
#define LSM6DSM_TAP_CFG                  (uint8_t)0x58U  /**< interrupt, inactivity function , filtering (r/w) **/
#define LSM6DSM_TAP_THS_6D               (uint8_t)0x59U  /**< Portrait/landscape, tap function threshold (r/w) **/
#define LSM6DSM_INT_DUR2                 (uint8_t)0x5AU  /**< Tap recognition function setting (r/w) **/
#define LSM6DSM_WAKE_UP_THS              (uint8_t)0x5BU  /**< Single and double-tap function threshold (r/w) **/
#define LSM6DSM_WAKE_UP_DUR              (uint8_t)0x5CU  /**< Free-fall, wakeup, timestamp, sleep duration (r/w) **/
#define LSM6DSM_FREE_FALL                (uint8_t)0x5DU  /**< Free-fall function duration setting register (r/w) **/
#define LSM6DSM_MD1_CFG                  (uint8_t)0x5EU  /**< Functions routing on INT1 register (r/w) **/
#define LSM6DSM_MD2_CFG                  (uint8_t)0x5FU  /**< Functions routing on INT2 register (r/w) **/

#define LSM6DSM_MASTER_CMD_CODE          (uint8_t)0x60U  /**< Master command code used for stamping **/
#define LSM6DSM_SENS_SYNC_SPI_ERROR_CODE (uint8_t)0x61U  /**< Error code used for sensor synchronization **/

#define LSM6DSM_OUT_MAG_RAW_X_L          (uint8_t)0x66U  /**< External magnetometer X data - Bit 7..0 (r) **/
#define LSM6DSM_OUT_MAG_RAW_X_H          (uint8_t)0x67U  /**< External magnetometer X data - Bit 15..8 (r) **/
#define LSM6DSM_OUT_MAG_RAW_Y_L          (uint8_t)0x68U  /**< External magnetometer Y data - Bit 7..0 (r) **/
#define LSM6DSM_OUT_MAG_RAW_Y_H          (uint8_t)0x69U  /**< External magnetometer Y data - Bit 15..8 (r) **/
#define LSM6DSM_OUT_MAG_RAW_Z_L          (uint8_t)0x6AU  /**< External magnetometer Z data - Bit 7..0 (r) **/
#define LSM6DSM_OUT_MAG_RAW_Z_H          (uint8_t)0x6BU  /**< External magnetometer Z data - Bit 15..8 (r) **/

#define LSM6DSM_INT_OIS                  (uint8_t)0x6FU  /**< OIS interrupt config (r) (AUX SPI (r/w)) **/
#define LSM6DSM_CTRL1_OIS                (uint8_t)0x70U  /**< OIS configuration 1 (r) (AUX SPI (r/w)) **/
#define LSM6DSM_CTRL2_OIS                (uint8_t)0x71U  /**< OIS configuration 2 (r) (AUX SPI (r/w)) **/
#define LSM6DSM_CTRL3_OIS                (uint8_t)0x72U  /**< OIS configuration 3 (r) (AUX SPI (r/w)) **/

#define LSM6DSM_X_OFS_USR                (uint8_t)0x73U  /**< Accelerometer X user offset correction (r/w) **/
#define LSM6DSM_Y_OFS_USR                (uint8_t)0x74U  /**< Accelerometer Y user offset correction (r/w) **/
#define LSM6DSM_Z_OFS_USR                (uint8_t)0x75U  /**< Accelerometer Z user offset correction (r/w) **/
/*! @} */

/*! @defgroup lsm6dsm_embedded_registers Group: embedded registers   
*  lsm6dsm embedded registers
*  @{
*/
#define LSM6DSM_SLV0_ADD                 (uint8_t)0x02U /**< I2C slave address Sensor   1 (r/w) **/
#define LSM6DSM_SLV0_SUBADD              (uint8_t)0x03U /**< Address of register Sensor 1 (r/w) **/
#define LSM6DSM_SLAVE0_CONFIG            (uint8_t)0x04U /**< configuration Sensor 1, sensor hub (r/w) **/
#define LSM6DSM_SLV1_ADD                 (uint8_t)0x05U /**< I2C slave address Sensor   2 (r/w) **/
#define LSM6DSM_SLV1_SUBADD              (uint8_t)0x06U /**< Address of register Sensor 2 (r/w) **/
#define LSM6DSM_SLAVE1_CONFIG            (uint8_t)0x07U /**< configuration Sensor       2 (r/w) **/
#define LSM6DSM_SLV2_ADD                 (uint8_t)0x08U /**< I2C slave address Sensor   3 (r/w) **/
#define LSM6DSM_SLV2_SUBADD              (uint8_t)0x09U /**< Address of register Sensor 3 (r/w) **/
#define LSM6DSM_SLAVE2_CONFIG            (uint8_t)0x0AU /**< configuration Sensor       3 (r/w) **/
#define LSM6DSM_SLV3_ADD                 (uint8_t)0x0BU /**< I2C slave address Sensor   4 (r/w) **/
#define LSM6DSM_SLV3_SUBADD              (uint8_t)0x0CU /**< Address of register Sensor 4 (r/w) **/
#define LSM6DSM_SLAVE3_CONFIG            (uint8_t)0x0DU /**< configuration Sensor       4 (r/w) **/

#define LSM6DSM_DATAWR_SRC_MODE_SUB_SLV0 (uint8_t)0x0EU /**< Write in SLV0 register  (r/w) **/
#define LSM6DSM_CONFIG_PEDO_THS_MIN      (uint8_t)0x0FU /**< Pedometer configuration (r/w) **/

#define LSM6DSM_SM_THS                   (uint8_t)0x13U /**< Significant motion configuration (r/w) **/
#define LSM6DSM_PEDO_DEB_REG             (uint8_t)0x14U /**< Pedometer debounce configuration (r/w) **/
#define LSM6DSM_STEP_COUNT_DELTA         (uint8_t)0x15U /**< Time period step detection  (r/w) **/

#define LSM6DSM_MAG_SI_XX                (uint8_t)0x24U /**< Soft-iron matrix correction row,col 1,1 (r/w)  **/
#define LSM6DSM_MAG_SI_XY                (uint8_t)0x25U /**< Soft-iron matrix correction row,col 1,2 (r/w)  **/
#define LSM6DSM_MAG_SI_XZ                (uint8_t)0x26U /**< Soft-iron matrix correction row,col 1,3 (r/w)  **/
#define LSM6DSM_MAG_SI_YX                (uint8_t)0x27U /**< Soft-iron matrix correction row,col 2,1 (r/w)  **/
#define LSM6DSM_MAG_SI_YY                (uint8_t)0x28U /**< Soft-iron matrix correction row,col 2,2 (r/w)  **/
#define LSM6DSM_MAG_SI_YZ                (uint8_t)0x29U /**< Soft-iron matrix correction row,col 2,3 (r/w)  **/
#define LSM6DSM_MAG_SI_ZX                (uint8_t)0x2AU /**< Soft-iron matrix correction row,col 3,1 (r/w)  **/
#define LSM6DSM_MAG_SI_ZY                (uint8_t)0x2BU /**< Soft-iron matrix correction row,col 3,2 (r/w)  **/
#define LSM6DSM_MAG_SI_ZZ                (uint8_t)0x2CU /**< Soft-iron matrix correction row,col 3,3 (r/w)  **/

#define LSM6DSM_MAG_OFFX_L               (uint8_t)0x2DU /**< Offset X-axis hard-iron compensation Bit 7..0 (r/w)**/
#define LSM6DSM_MAG_OFFX_H               (uint8_t)0x2EU /**< Offset X-axis hard-iron compensation Bit 15..8 (r/w)**/
#define LSM6DSM_MAG_OFFY_L               (uint8_t)0x2FU /**< Offset Y-axis hard-iron compensation Bit 7..0 (r/w)**/
#define LSM6DSM_MAG_OFFY_H               (uint8_t)0x30U /**< Offset Y-axis hard-iron compensation Bit 15..8 (r/w)**/
#define LSM6DSM_MAG_OFFZ_L               (uint8_t)0x31U /**< Offset Z-axis hard-iron compensation Bit 7..0 (r/w)**/
#define LSM6DSM_MAG_OFFZ_H               (uint8_t)0x32U /**< Offset Z-axis hard-iron compensation Bit 15..8 (r/w)**/

#define LSM6DSM_A_WRIST_TILT_LAT         (uint8_t)0x50U /**< Absolute Wrist Tilt latency (r/w) **/
#define LSM6DSM_A_WRIST_TILT_THS         (uint8_t)0x54U /**< Absolute Wrist Tilt threshold (r/w)**/
#define LSM6DSM_A_WRIST_TILT_MASK        (uint8_t)0x59U /**< Absolute Wrist Tilt mask (r/w)**/

/*! @} */

/*! @defgroup lsm6dsm_settings Group: settings    
*  lsm6dsm settings
*  @{
*/

/*! @enum enable_disable_t Enum: disable     
* @{
*/
typedef enum{
  LSM6DSM_ENABLE  = 1,   /**< Enable Register  **/
  LSM6DSM_DISABLE = 0,  /**< Disable Register **/
} lsm6dsm_en_dis_t;

/*! @} */

/*! @enum lsm6dsm_func_cfg_en_t Enum: func cfg en   
 *      Enable access to the embedded functions configuration registers
 *  @{
 */
typedef enum {
  LSM6DSM_BANK_A_B_DISABLED  = 0,
  LSM6DSM_BANK_A             = 4,
  LSM6DSM_BANK_B             = 5,
} lsm6dsm_func_cfg_en_t; 
/*! @} */

/*! @enum lsm6dsm_rr_t Enum: rr     
 *  @{
 */
typedef enum {
  LSM6DSM_RES_RATIO_2_11  = 0,
  LSM6DSM_RES_RATIO_2_12  = 1,
  LSM6DSM_RES_RATIO_2_13  = 2,
  LSM6DSM_RES_RATIO_2_14  = 3,
} lsm6dsm_rr_t; 
/*! @} */

/*! @enum lsm6dsm_decimation_factor_t Enum: decimation factor  
 *  @{
 */
typedef enum {
  LSM6DSM_NOT_IN_FIFO       = 0,
  LSM6DSM_NO_DECIMATION     = 1,
  LSM6DSM_FAC_DECIMATION_2  = 2,
  LSM6DSM_FAC_DECIMATION_3  = 3,
  LSM6DSM_FAC_DECIMATION_4  = 4,
  LSM6DSM_FAC_DECIMATION_8  = 5,
  LSM6DSM_FAC_DECIMATION_16 = 6,
  LSM6DSM_FAC_DECIMATION_32 = 7,
} lsm6dsm_decimation_factor_t;
/*! @} */

/*! @enum lsm6dsm_fifo_mode_t Enum: fifo mode    
 *  @{
 */
typedef enum {
  LSM6DSM_BYPASS_MODE           = 0,
  LSM6DSM_FIFO_MODE             = 1,
  LSM6DSM_STREAM_TO_FIFO_MODE   = 3,
  LSM6DSM_BYPASS_TO_STREAM_MODE = 4,
  LSM6DSM_STREAM_MODE           = 6,
} lsm6dsm_fifo_mode_t; 
/*! @} */

/*! @enum lsm6dsm_odr_fifo_t Enum: odr fifo    
 *  @{
 */
typedef enum {
  LSM6DSM_FIFO_DISABLE   =  0,
  LSM6DSM_FIFO_12Hz5     =  1,
  LSM6DSM_FIFO_26Hz      =  2,
  LSM6DSM_FIFO_52Hz      =  3,
  LSM6DSM_FIFO_104Hz     =  4,
  LSM6DSM_FIFO_208Hz     =  5,
  LSM6DSM_FIFO_416Hz     =  6,
  LSM6DSM_FIFO_833Hz     =  7,
  LSM6DSM_FIFO_1k66Hz    =  8,
  LSM6DSM_FIFO_3k33Hz    =  9,
  LSM6DSM_FIFO_6k66Hz    = 10,
} lsm6dsm_odr_fifo_t; 
/*! @} */

/** CTRL1_XL (10h)
 * @brief  Linear acceleration sensor register Output data rate and power mode selection
 */

/*! @enum lsm6dsm_odr_xl_t Enum: odr xl    
 *  @{
 */
typedef enum {
  LSM6DSM_XL_ODR_POWER_DOWN =  0,
  LSM6DSM_XL_ODR_1Hz6       = 11,
  LSM6DSM_XL_ODR_12Hz5      =  1,
  LSM6DSM_XL_ODR_26Hz       =  2,
  LSM6DSM_XL_ODR_52Hz       =  3,
  LSM6DSM_XL_ODR_104Hz      =  4,
  LSM6DSM_XL_ODR_208Hz      =  5,
  LSM6DSM_XL_ODR_416Hz      =  6,
  LSM6DSM_XL_ODR_833Hz      =  7,
  LSM6DSM_XL_ODR_1k66Hz     =  8,
  LSM6DSM_XL_ODR_3k33Hz     =  9,
  LSM6DSM_XL_ODR_6k66Hz     = 10,
} lsm6dsm_odr_xl_t; 
/*! @} */

/*! @enum lsm6dsm_fs_xl_t Enum: fs xl    
 *  @{
 */
typedef enum {
  LSM6DSM_2g       = 0,
  LSM6DSM_4g       = 2,
  LSM6DSM_8g       = 3,
  LSM6DSM_16g      = 1,
} lsm6dsm_fs_xl_t; 
/*! @} */

/*! @enum lsm6dsm_bw0_xl_t Enum: bw0 xl    
 *  @{
 */
typedef enum {
  LSM6DSM_BW_1_5_KHZ = 0,
  LSM6DSM_BW_400_HZ  = 1,
} lsm6dsm_bw0_xl_t; 
/*! @} */


/** CTRL2_G (11h)
 * @brief Angular rate sensor control, Gyroscope output data rate selection
 */

/*! @enum lsm6dsm_odr_g_t Enum: odr g    
 *  @{
 */
typedef enum {
  LSM6DSM_GY_ODR_OFF    =  0,
  LSM6DSM_GY_ODR_12Hz5  =  1,
  LSM6DSM_GY_ODR_26Hz   =  2,
  LSM6DSM_GY_ODR_52Hz   =  3,
  LSM6DSM_GY_ODR_104Hz  =  4,
  LSM6DSM_GY_ODR_208Hz  =  5,
  LSM6DSM_GY_ODR_416Hz  =  6,
  LSM6DSM_GY_ODR_833Hz  =  7,
  LSM6DSM_GY_ODR_1k66Hz =  8,
  LSM6DSM_GY_ODR_3k33Hz =  9,
  LSM6DSM_GY_ODR_6k66Hz = 10,
} lsm6dsm_odr_g_t; 
/*! @} */


/*! @enum lsm6dsm_fs_g_t Enum: fs g    
 *  @{
 */
typedef enum {
  LSM6DSM_GY_FS_250_DPS  =  0,
  LSM6DSM_GY_FS_500_DPS  =  1,
  LSM6DSM_GY_FS_1000_DPS =  2,
  LSM6DSM_GY_FS_2000_DPS =  3,
} lsm6dsm_fs_g_t; 
/*! @} */

/** CTRL3_G (12h)
 */
/*! @enum lsm6dsm_ble_t Enum: ble     
 *  @{
 */
typedef enum {
  LSM6DSM_LITTLE_ENDIAN_DATA  =  0,
  LSM6DSM_BIG_ENDIAN_DATA     =  1,
} lsm6dsm_ble_t; 
/*! @} */

/*! @enum lsm6dsm_sim_t Enum: sim     
 *  @{
 */
typedef enum {
  LSM6DSM_4_WIRE_SPI     =  0, /*< default value, 4-wire spi and i2c enable */
  LSM6DSM_3_WIRE_SPI     =  1, /*< 3-wire spi enable */
} lsm6dsm_sim_t; 
/*! @} */

/*! @enum lsm6dsm_pp_od_t Enum: pp od    
 *  @{
 */
typedef enum {
  LSM6DSM_INT2_PUSH_PULL     =  0,
  LSM6DSM_INT2_OPEN_DRAIN    =  1,
} lsm6dsm_pp_od_t; 
/*! @} */

/*! @enum lsm6dsm_h_lactive_t Enum: h lactive
 *  @{
 */
typedef enum {
  LSM6DSM_INT_OUT_ACTIVE_HIGH   =  0,
  LSM6DSM_INT_OUT_ACTIVE_LOW    =  1,
} lsm6dsm_h_lactive_t; 
/*! @} */

/*! @enum lsm6dsm_bdu_t Enum: bdu     
 *  @{
 */
typedef enum {
  LSM6DSM_OUT_REG_UPDATE_CONT           =  0,
  LSM6DSM_OUT_REG_UPDATE_LSB_MSB_READ   =  1,
} lsm6dsm_bdu_t; 
/*! @} */


/** CTRL5_C (14h)
 */
/*! @enum lsm6dsm_st_xl_t Enum: st xl
 *  @{
 */
typedef enum {
  LSM6DSM_XL_SELF_TEST_DISABLE    = 0,
  LSM6DSM_XL_SELF_TEST_POS_SIGN   = 1,
  LSM6DSM_XL_SELF_TEST_NEG_SIGN   = 2,
} lsm6dsm_st_xl_t; 
/*! @} */ 

/*! @enum lsm6dsm_st_g_t Enum: st g
 *  @{
 */
typedef enum {
  LSM6DSM_GY_SELF_TEST_DISABLE    = 0,
  LSM6DSM_GY_SELF_TEST_POS_SIGN   = 1,
  LSM6DSM_GY_SELF_TEST_NEG_SIGN   = 3,
} lsm6dsm_st_g_t;  
/*! @} */

/*! @enum lsm6dsm_den_lh_t Enum: den lh    
 *  @{
 */
typedef enum {
  LSM6DSM_DEN_ACT_LOW    = 0,
  LSM6DSM_DEN_ACT_HIGH   = 1,
} lsm6dsm_den_lh_t; 
/*! @} */

/*! @enum lsm6dsm_rounding_t Enum: rounding     
 *  @{
 */
typedef enum {
  LSM6DSM_ROUND_DISABLE            = 0,
  LSM6DSM_ROUND_XL_ONLY            = 1,
  LSM6DSM_ROUND_GY_ONLY            = 2,
  LSM6DSM_ROUND_GY_XL              = 3,
  LSM6DSM_ROUND_REG_SH1_TO_SH6     = 4, //Registers from SENSORHUB1_REG (2Eh) to SENSORHUB6_REG (33h)
  LSM6DSM_ROUND_XL_REG_SH1_TO_SH6  = 5,
  LSM6DSM_ROUND_GY_XL_REG_SH1_TO_SH12  = 6,
  LSM6DSM_ROUND_GY_XL_REG_SH1_TO_SH6   = 7,
} lsm6dsm_rounding_t; 
/*! @} */

/** CTRL6_C (15h)
 * @brief Gyroscope's low-pass filter (LPF1) bandwidth selection
 */

/*! @enum lsm6dsm_lp_g_bw0_sel_t Enum: lp g bw0 sel  
 *  @{
 */
typedef enum {
  LSM6DSM_LP_G_BW_LVL0 = 0,
  LSM6DSM_LP_G_BW_LVL1 = 1,
  LSM6DSM_LP_G_BW_LVL2 = 2,
  LSM6DSM_LP_G_BW_LVL3 = 3,
} lsm6dsm_lp_g_bw0_sel_t; 
/*! @} */

/*! @enum lsm6dsm_den_mode_t Enum: den mode    
 *  @{
 */
typedef enum {
  LSM6DSM_DEN_DISABLE    = 0,
  LSM6DSM_LEVEL_FIFO     = 6,
  LSM6DSM_LEVEL_LATCHED  = 3,
  LSM6DSM_LEVEL_TRIGGER  = 2,
  LSM6DSM_EDGE_TRIGGER   = 4,
} lsm6dsm_den_mode_t; 
/*! @} */

/*! @enum lsm6dsm_usr_off_w_t Enum: usr off w   
 *  @{
 */
typedef enum {
  LSM6DSM_W_XL_2E_M10    = 0,
  LSM6DSM_W_XL_2E_M6     = 1,
} lsm6dsm_usr_off_w_t; 
/*! @} */

/** CTRL7_C (16h)
 * @brief Gyroscope digital HP filter cutoff selection
 */

/*! @enum lsm6dsm_hpm_g_cutoff_t Enum: hpm g cutoff
 *  @{
 */
typedef enum {
  LSM6DSM_HP_16mHz_LP2      = 0,
  LSM6DSM_HP_65mHz_LP2      = 1,
  LSM6DSM_HP_260mHz_LP2     = 2,
  LSM6DSM_HP_1Hz04_LP2      = 3,
} lsm6dsm_hpm_g_cutoff_t; 
/*! @} */

/** CTRL9_XL (18h)
 */

/*! @enum lsm6dsm_den_xl_g_t Enum: den xl g   
 *  @{
 */
typedef enum {
  LSM6DSM_DEN_PIN_GY         = 0,
  LSM6DSM_DEN_PIN_XL         = 1,
} lsm6dsm_den_xl_g_t; 
/*! @} */

/** MASTER_CONFIG (1Ah)
 */

/*! @enum lsm6dsm_data_valid_sel_fifo_t Enum: data valid sel fifo
 *  @{
 */
typedef enum {
  LSM6DSM_DV_XL_GY_ST = 0,
  LSM6DSM_DV_XL_GY_SENSORHUB_DR = 1,
} lsm6dsm_data_valid_sel_fifo_t; 
/*! @} */

/*! @enum lsm6dsm_start_config_t Enum: start config
 *  @{
 */
typedef enum {
  LSM6DSM_XL_GY_DRDY        = 0,
  LSM6DSM_EXT_ON_INT2_PIN   = 1,
} lsm6dsm_start_config_t; 
/*! @} */

/** TAP_CFG (58h)
 */
/*! @enum lsm6dsm_inact_en_t Enum: inact en
 *  @{
 */
typedef enum {
  LSM6DSM_PROPERTY_DISABLE          = 0,
  LSM6DSM_XL_12Hz5_GY_NOT_AFFECTED  = 1,
  LSM6DSM_XL_12Hz5_GY_SLEEP         = 2,
  LSM6DSM_XL_12Hz5_GY_PD            = 3,
} lsm6dsm_inact_en_t; 
/*! @} */

/*! @enum lsm6dsm_hpf_slope_t Enum: hpf slope    
 *  @{
 */
typedef enum {
  LSM6DSM_FILTER_SLOPE          = 0,
  LSM6DSM_FILTER_HPF            = 1,
} lsm6dsm_hpf_slope_t; 
/*! @} */

/** WAKE_UP_DUR (5Ch)
 */
/*! @enum lsm6dsm_timer_hr_t Enum: timer hr
 *  @{
 */
typedef enum {
  LSM6DSM_LSB_6ms4    = 0,
  LSM6DSM_LSB_25us    = 1,
} lsm6dsm_timer_hr_t; 
/*! @} */

/** FREE_FALL (5Dh)
 */
/*! @enum lsm6dsm_ff_ths_t Enum: ff ths    
 *  @{
 */
typedef enum {
  LSM6DSM_FF_TSH_156mg = 0,
  LSM6DSM_FF_TSH_219mg = 1,
  LSM6DSM_FF_TSH_250mg = 2,
  LSM6DSM_FF_TSH_312mg = 3,
  LSM6DSM_FF_TSH_344mg = 4,
  LSM6DSM_FF_TSH_406mg = 5,
  LSM6DSM_FF_TSH_469mg = 6,
  LSM6DSM_FF_TSH_500mg = 7,
} lsm6dsm_ff_ths_t; 
/*! @} */

/** FREE_FALL (70h)
 */
/*! @enum lsm6dsm_fs_g_ois_t Enum: fs g ois   
 *  @{
 */
typedef enum {
  LSM6DSM_250dps_AUX   = 0,
  LSM6DSM_125dps_AUX   = 1,
  LSM6DSM_500dps_AUX   = 2,
  LSM6DSM_1000dps_AUX  = 4,
  LSM6DSM_2000dps_AUX  = 6,
} lsm6dsm_fs_g_ois_t; 
/*! @} */

/** CTRL2_OIS (71h)
 */
/*! @enum lsm6dsm_hpm_ois_t Enum: hpm ois    
 *  @{
 */
typedef enum {
  LSM6DSM_HP_16_mHz   = 0,
  LSM6DSM_HP_65_mHz   = 1,
  LSM6DSM_HP_260_mHz   = 2,
  LSM6DSM_HP_1_04_Hz   = 3,
} lsm6dsm_hpm_ois_t; 
/*! @} */

/*! @enum lsm6dsm_ftype_ois_t Enum: ftype ois
 *  @{
 */
typedef enum {
  LSM6DSM_BW_173_Hz   = 2,
  LSM6DSM_BW_237_Hz   = 1,
  LSM6DSM_BW_351_Hz   = 0,
  LSM6DSM_BW_937_Hz   = 3,
} lsm6dsm_ftype_ois_t; 
/*! @} */

/*! @} */

/*! @defgroup lsm6dsm_structs Group: structs    
*  lsm6dsm structs
*  @{
*/

typedef struct {
  uint8_t                   : 5;  /**< RESERVED **/
  uint8_t func_cfg_en       : 3;  /**< Enable access to the embedded functions config @ref lsm6dsm_func_cfg_en_t **/
} lsm6dsm_func_cfg_access_t;

typedef struct {
  uint8_t tph                 : 4;  /**< Sensor synchronization time. frame step 500 ms, full range 5 s **/
  uint8_t                     : 4;  /**< RESERVED **/
} lsm6dsm_sensor_sync_time_frame_t;

typedef struct {
  uint8_t res_ratio           : 2;  /**< Resolution ratio of error code for sensor synchronization @ref lsm6dsm_rr_t**/
  uint8_t                     : 6;  /**< RESERVED **/
} lsm6dsm_sensor_sync_res_ratio_t;

typedef struct {
  uint8_t fth                  : 8;  /**< FIFO threshold level setting BIT 7..0 
                                          Minimum resolution for the FIFO is 1LSB = 2 byte**/
} lsm6dsm_fifo_ctrl1_t;

typedef struct {
  uint8_t fth                  : 3;  /**< FIFO threshold level setting 
                                            Minimum resolution for the FIFO is 1LSB = 2 byte**/
  uint8_t fifo_temp_en         : 1;  /**< Enable the temperature data storage in FIFO **/
  uint8_t                      : 2;  /**< RESERVED **/
  uint8_t timer_pedo_fifo_drdy : 1;  /**< enable write in FIFO at every step **/
  uint8_t timer_pedo_fifo_en   : 1;  /**< Enable pedometer step counter and timestamp as fifo 4th dataset **/
} lsm6dsm_fifo_ctrl2_t;

typedef struct {
  uint8_t dec_fifo_xl         : 3;  /**< Gyro FIFO (first data set) decimation factor  @ref lsm6dsm_decimation_factor_t **/
  uint8_t dec_fifo_gyro       : 3;  /**< Acc FIFO (second data set) decimation setting @ref lsm6dsm_decimation_factor_t **/
  uint8_t                     : 2;  /**< RESERVED **/
} lsm6dsm_fifo_ctrl3_t;

typedef struct {
  uint8_t dec_ds3_fifo        : 3;  /**< Third FIFO data set decimation setting  @ref lsm6dsm_decimation_factor_t **/
  uint8_t dec_ds4_fifo        : 3;  /**< Fourth FIFO data set decimation setting  @ref lsm6dsm_decimation_factor_t **/
  uint8_t only_high_data      : 1;  /**< Enable MSByte only memorization in FIFO for XL and Gyro in FIFO**/
  uint8_t stop_on_fth         : 1;  /**< Enable FIFO threshold lvl 1: FIFO depth is limited to threshold level**/
} lsm6dsm_fifo_ctrl4_t;

typedef struct {
  uint8_t fifo_mode          : 3;  /**< FIFO mode selection @ref lsm6dsm_fifo_mode_t **/
  uint8_t odr_fifo           : 4;  /**< FIFO ODR selection  @ref lsm6dsm_odr_fifo_t **/
  uint8_t                    : 1;  /**< RESERVED **/
} lsm6dsm_fifo_ctrl5_t;

typedef struct {
  uint8_t int2_wrist_tilt    : 1;  /**< Enable Wrist tilt interrupt on INT2 pad **/
  uint8_t                    : 6;  /**< RESERVED **/
  uint8_t drdy_pulsed        : 1;  /**< Enable pulsed DataReady mode **/
} lsm6dsm_drdy_pulse_cfg_t;

typedef struct {
                                        /**< @brief Enable the following interrupts on pad INT1: **/
  uint8_t drdy_xl           : 1;  /**< Accelerometer Data Ready   **/
  uint8_t drdy_g            : 1;  /**< Significant motion interrupt **/
  uint8_t boot              : 1;  /**< FIFO full flag interrupt **/
  uint8_t fth               : 1;  /**< FIFO overrun interrupt **/
  uint8_t fifo_ovr          : 1;  /**< FIFO threshold interrupt  **/
  uint8_t full_flag         : 1;  /**< Boot status available  **/
  uint8_t sign_mot          : 1;  /**< Gyroscope Data Ready **/
  uint8_t step_detector     : 1;  /**< Pedometer step recognition  **/
} lsm6dsm_int1_ctrl_t;

typedef struct {              /**< @brief Enable the following interrupts on pad INT2: **/
  uint8_t drdy_xl           : 1;  /**< Accelerometer Data Ready **/
  uint8_t drdy_g            : 1;  /**< Gyroscope Data Ready **/
  uint8_t drdy_temp         : 1;  /**< Temperature Data Ready **/
  uint8_t fth               : 1;  /**< FIFO threshold interrupt **/
  uint8_t fifo_ovr          : 1;  /**< FIFO overrun interrupt **/
  uint8_t full_flag         : 1;  /**< FIFO full flag interrupt **/
  uint8_t step_count_ov     : 1;  /**< Step counter overflow interrupt  **/
  uint8_t step_delta        : 1;  /**< Pedometer step recognition interrupt **/
} lsm6dsm_int2_ctrl_t;

//WHO_AM_I (0Fh) 

typedef struct {
  uint8_t bw0_xl             : 1;  /**< Accelerometer analog chain bandwidth (ODR ≥ 1.67 kHz). @ref lsm6dsm_bw0_xl_t **/
  uint8_t lpf1_bw_sel        : 1;  /**< Accelerometer digital LPF (LPF1) bandwidth selection. @ref lsm6dsm_ctrl8_xl_t **/
  uint8_t fs_xl              : 2;  /**< Accelerometer full-scale selection. @ref lsm6dsm_fs_xl_t **/
  uint8_t odr_xl             : 4;  /**< Output data rate and power mode selection @ref lsm6dsm_odr_xl_t **/
} lsm6dsm_ctrl1_xl_t;

typedef struct {
  uint8_t                    : 1;  /**< RESERVED **/
  uint8_t fs_125             : 1;  /**< Gyroscope full-scale at 125 dps enable**/
  uint8_t fs_g               : 2;  /**< Gyroscope full-scale selection @ref lsm6dsm_fs_g_t **/
  uint8_t odr_g              : 4;  /**< Gyroscope output data rate selection @ref lsm6dsm_odr_g_t **/
} lsm6dsm_ctrl2_g_t;

typedef struct {
  uint8_t sw_reset           : 1;  /**< Software reset.  1: reset device. This bit is automatically cleared.**/
  uint8_t ble                : 1;  /**< Big/Little Endian Data selection. @ref lsm6dsm_ble_t **/
  uint8_t if_inc             : 1;  /**< Register address automatically incremented serial access. 1: enable **/
  uint8_t sim                : 1;  /**< SPI Serial Interface Mode selection @ref lsm6dsm_sim_t **/
  uint8_t pp_od              : 1;  /**< Push-pull/open-drain selection on INT1 and INT2 pads @ref lsm6dsm_pp_od_t **/
  uint8_t h_lactive          : 1;  /**< Interrupt Output activation level @ref lsm6dsm_h_lactive_t**/
  uint8_t bdu                : 1;  /**< Block Data Update continuous / updated when MSB and LSB read @ref lsm6dsm_bdu_t **/
  uint8_t boot               : 1;  /**< Reboots memory content 0: normal mode **/
} lsm6dsm_ctrl3_c_t;

typedef struct {
  uint8_t                    : 1;  /**< RESERVED **/
  uint8_t lpf1_sel_g         : 1;  /**< Enable gyroscope digital LPF1 if auxiliary SPI is disabled **/
  uint8_t i2c_disable        : 1;  /**< Disable I2C interface. 0: I2C enabled **/
  uint8_t drdy_mask          : 1;  /**< Configuration 1 data available enable. 0: DA timer disabled; **/
  uint8_t den_drdy_int1      : 1;  /**< DEN DRDY signal on INT1 pad. 0: signal on INT1  disabled**/
  uint8_t int2_on_int1       : 1;  /**< All interrupt signals available on INT1 pad instead of INT1 INT2 enable 
                                           0: interrupt signals on both INT1 and INT2 pads **/
  uint8_t sleep              : 1;  /**< Gyroscope sleep mode enable. 0: disabled **/
  uint8_t den_xl_en          : 1;  /**< Extend DEN functionality to accelerometer sensor. 0: disabled  **/
} lsm6dsm_ctrl4_c_t;

typedef struct {
  uint8_t st_xl              : 2;  /**< Linear acceleration sensor self-test enable @ref lsm6dsm_st_xl_t **/
  uint8_t st_g               : 2;  /**< Angular rate sensor self-test enable @ref lsm6dsm_st_g_t **/
  uint8_t den_lh             : 1;  /**< DEN active level configuration @ref lsm6dsm_den_lh_t **/
  uint8_t rounding           : 3;  /**< Circular burst-mode (rounding) read from output registers 
                                        through the primary interface. Rounding Pattern: @ref lsm6dsm_rounding_t 
                                        (if enabled, automatically return to initial address of selection after full read)**/
} lsm6dsm_ctrl5_c_t;

typedef struct {
  uint8_t ftype               : 2;  /**< Gyroscope's low-pass filter (LPF1) bandwidth selection @ref lsm6dsm_lp_g_bw0_sel_t **/ 
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t usr_off_w           : 1;  /**< Weight of XL user offset bits of registers X_OFS_USR (73h), Y_OFS_USR (74h),
                                      Z_OFS_USR (75h) @ref  lsm6dsm_usr_off_w_t **/
  uint8_t xl_hm_mode          : 1;  /**< High-performance mode disable for accelerometer (0: high-perf. mode enabled) **/
  uint8_t den_data_level_mode : 3;  /**< DEN data edge-level-sensitive trigger settings @ref lsm6dsm_den_mode_t **/ 
} lsm6dsm_ctrl6_c_t;

typedef struct {
  uint8_t                     : 2;  /**< RESERVED **/
  uint8_t rounding_status     : 1;  /**< Source register rounding function on WAKE_UP_SRC, TAP_SRC, D6D_SRC, STATUS_REG,
                                        and FUNC_SRC registers in the primary interface. 0: Rounding disabled **/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t hpm_g               : 2;  /**< Gyroscope digital HP filter cutoff selection @ref lsm6dsm_hpm_g_cutoff_t**/
  uint8_t hp_en_g             : 1;  /**< Gyroscope digital high-pass filter enable.     0: HPF disabled**/
  uint8_t g_hm_mode           : 1;  /**< High-perf. operating mode disable for gyro(1). 0: high-perf. operating mode enabled **/
} lsm6dsm_ctrl7_g_t;

typedef struct {      /**< @brief refer to datasheet for struct setup**/
  uint8_t low_pass_on_6d      : 1;  /**< LPF2 on 6D function selection. **/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t hp_slope_xl_en      : 1;  /**< Accelerometer slope filter / high-pass filter selection.  **/
  uint8_t input_composite     : 1;  /**< Composite filter input selection. **/
  uint8_t hp_ref_mode         : 1;  /**< Enable HP filter reference mode. **/
  uint8_t hpcf_xl             : 2;  /**< Accelerometer LPF2 and high-pass filter configuration and cutoff setting. **/
  uint8_t lpf2_xl_en          : 1;  /**< Accelerometer low-pass filter LPF2 path selection  **/
} lsm6dsm_ctrl8_xl_t;

typedef struct {
  uint8_t                     : 2;  /**< RESERVED **/
  uint8_t soft_en             : 1;  /**< EN soft-iron correction algorithm magnetometer. 0: soft-iron corr. algo. disabled **/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t den_xl_g            : 1;  /**< DEN stamping sensor selection @ref lsm6dsm_den_xl_g_t**/
  uint8_t den_z               : 1;  /**< DEN value enable storage in LSB of Z-axis.  1: DEN stored in Z-axis LSB **/
  uint8_t den_y               : 1;  /**< DEN value enable storage in LSB of Y-axis.  1: DEN stored in Y-axis LSB **/
  uint8_t den_x               : 1;  /**< DEN value enable storage in LSB of X-axis.  1: DEN stored in X-axis LSB **/
} lsm6dsm_ctrl9_xl_t;

typedef struct {
  uint8_t sign_motion_en      : 1;  /**< Enable significant motion detection function. 0: disabled; **/
  uint8_t pedo_rst_step       : 1;  /**< Reset pedometer step counter. 0: disabled **/
  uint8_t func_en             : 1;  /**< Enable embedded funct. (pedometer, tilt, wrist tilt, significant motion detection,
                                           sensor hub and ironing). 0: disable funct. of embed func. and acc. filters**/
  uint8_t tilt_en             : 1;  /**< Enable tilt calculation. 0: tilt calculation disabled. req. func_en=1 **/
  uint8_t pedo_en             : 1;  /**< Enable pedometer algorithm. 0: pedometer algorithm disabled**/
  uint8_t timer_en            : 1;  /**< Enable timestamp count.  0: timestamp count disabled**/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t wrist_tilt_en       : 1;  /**< Enable wrist tilt algorithm. 0: wrist tilt algorithm disabled**/
} lsm6dsm_ctrl10_c_t;

typedef struct {
  uint8_t master_on           : 1;  /**< Sensor hub I2C master enable 0: master I2C of sensor hub disabled **/
  uint8_t iron_en             : 1;  /**< Enable hard-iron correction algorithm magnetometer, 0:hard-iron algo. disabled**/
  uint8_t pass_through_mode   : 1;  /**< I2C interface pass-through enable, 0: pass-through disabled **/
  uint8_t pull_up_en          : 1;  /**< Auxiliary I2C pull-up enable, 0: internal pull-up on aux. I2C line disabled **/
  uint8_t start_config        : 1;  /**< Sensor Hub trigger signal selection. @ref lsm6dsm_start_config_t **/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t data_valid_sel_fifo : 1;  /**< Selection of FIFO data-valid signal  @ref lsm6dsm_data_valid_sel_fifo_t **/
  uint8_t drdy_on_int1        : 1;  /**< Manage the Master DRDY signal on INT1 pad. 0: disable Master DRDY on INT1 **/
} lsm6dsm_master_config_t;

typedef struct {
  uint8_t z_wu                : 1;  /**< Wakeup event detection status on Z-axis **/
  uint8_t y_wu                : 1;  /**< Wakeup event detection status on Y-axis **/
  uint8_t x_wu                : 1;  /**< Wakeup event detection status on X-axis **/
  uint8_t wu_ia               : 1;  /**< Wakeup event detection status **/
  uint8_t sleep_state_ia      : 1;  /**< Sleep event status **/
  uint8_t ff_ia               : 1;  /**< Free-fall event detection status. **/
  uint8_t                     : 2;  /**< RESERVED **/
} lsm6dsm_wake_up_src_t;

typedef struct {
  uint8_t z_tap               : 1;  /**< Tap event detection status on Z-axis. 1: tap event detected **/
  uint8_t y_tap               : 1;  /**< Tap event detection status on Y-axis. 1: tap event detected  **/
  uint8_t x_tap               : 1;  /**< Tap event detection status on X-axis. 1: tap event detected  **/
  uint8_t tap_sign            : 1;  /**< Sign of  tap event acceleration. 0: acceleration positive**/
  uint8_t double_tap          : 1;  /**< Double-tap event detection status. 0: double-tap not detected **/
  uint8_t single_tap          : 1;  /**< Single-tap event detection status. 0: single-tap not detected **/
  uint8_t tap_ia              : 1;  /**< Tap event detection status. 0: tap event not detected **/
  uint8_t                     : 1;  /**< RESERVED **/
} lsm6dsm_tap_src_t;

typedef struct {
  uint8_t xl                  : 1;  /**< X-axis low event (under threshold). 0: event not detected **/
  uint8_t xh                  : 1;  /**< X-axis high event (over threshold). 0: event not detected **/
  uint8_t yl                  : 1;  /**< Y-axis low event (under threshold). 0: event not detected **/
  uint8_t yh                  : 1;  /**< Y-axis high event (over threshold). 0: event not detected **/
  uint8_t zl                  : 1;  /**< Z-axis low event (under threshold). 0: event not detected **/
  uint8_t zh                  : 1;  /**< Z-axis high event (over threshold). 0: event not detected **/
  uint8_t d6d_ia              : 1;  /**< Interrupt for change position portrait, landscape, face-up,-down. 1: detected **/
  uint8_t den_drdy            : 1;  /**< DEN data-ready signal. high => data output coming from DEN active condition**/
} lsm6dsm_d6d_src_t;

typedef struct {            /**< @brief Read by primary interface I2C/SPI and auxiliary SPI  **/
  uint8_t xlda                : 1;  /**< Primary   SPI: Accelerometer new data available. 0: no set of data
                                    *   Secondary SPI: Accelerometer data available. 0: data was read **/

  uint8_t gda                 : 1;  /**< Primary   SPI: Gyroscope new data available. 0: no set of data  
                                    *   Secondary SPI: Gyroscope data available. 0: data was read  **/

  uint8_t tda_gyro_settling   : 1;  /**< Primary   SPI: TDA: Temperature new data available. 0: no set of data 
                                    *   Secondary SPI: GYRO_SETTLING: High when gyroscope output in settling phase **/ 
  uint8_t                     : 5;
} lsm6dsm_status_reg_status_spi_aux_t;

typedef struct {
  uint8_t diff_fifo           : 8;  /**<  Number of unread words (16-bit axes) stored in FIFO. BIT 7..0   **/ 
} lsm6dsm_fifo_status1_t;

typedef struct {
  uint8_t diff_fifo           : 3;  /**< Number of unread words (16-bit axes) stored in FIFO. BIT 10..8  **/  
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t fifo_empty          : 1;  /**< FIFO empty bit. 0: FIFO contains data **/
  uint8_t fifo_full_smart     : 1;  /**< Smart FIFO full status. 0: FIFO is not full **/
  uint8_t over_run            : 1;  /**< FIFO overrun status. 0: FIFO not completely filled **/
  uint8_t waterm              : 1;  /**< FIFO watermark status. watermark: bits FTH_[7:0], 0: FIFO lower than watermark **/
} lsm6dsm_fifo_status2_t;

typedef struct {
  uint8_t fifo_pattern          :8;  /**< Word of recursive pattern read at the next reading. BIT 7..0 **/
} lsm6dsm_fifo_status3_t;

typedef struct {
  uint8_t fifo_pattern        : 2;  /**< Word of recursive pattern read at the next reading. BIT 9..8 **/  
  uint8_t                     : 6;  /**< RESERVED **/
} lsm6dsm_fifo_status4_t;

typedef struct {
  uint8_t sensorhub_end_op    : 1;  /**< Sensor hub communication status. 0: sensor hub comm. not concluded **/
  uint8_t si_end_op           : 1;  /**< Hard/soft-iron calc. status. 0: Hard/soft-iron calc. not concluded **/
  uint8_t hi_fail             : 1;  /**< Fail in hard/soft-ironing algorithm. No fail. **/
  uint8_t step_overflow       : 1;  /**< Step counter overflow status. 0: step counter value < EXP(2,16) **/
  uint8_t step_detected       : 1;  /**< Step detector event detection  status. 0: event not detected **/
  uint8_t tilt_ia             : 1;  /**< Tilt event detection status. 0: tilt event not detected **/
  uint8_t sign_motion_ia      : 1;  /**< Significant motion event detection status. 0: event not detected**/
  uint8_t step_count_delta_ia : 1;  /**< Pedometer step recognition on delta time status. 
                                          0: no step recognized during delta time; **/
} lsm6dsm_func_src1_t;

typedef struct {
  uint8_t wrist_tilt_ia       : 1;  /**< Wrist tilt event detection status. 0: Wrist tilt event not detected**/
  uint8_t                     : 2;  /**< RESERVED **/
  uint8_t slave0_nack         : 1;  /**< set to 0 if Not acknowledge occurs on slave 0 communication.  **/
  uint8_t slave1_nack         : 1;  /**< set to 1 if Not acknowledge occurs on slave 0 communication.  **/
  uint8_t slave2_nack         : 1;  /**< set to 2 if Not acknowledge occurs on slave 0 communication. **/
  uint8_t slave3_nack         : 1;  /**< set to 3 if Not acknowledge occurs on slave 0 communication. **/
  uint8_t                     : 1;  /**< RESERVED **/
} lsm6dsm_func_src2_t;

typedef struct {
  uint8_t                     : 2;  /**< RESERVED **/
  uint8_t wrist_tilt_ia_zneg  : 1;  /**< Absolute Wrist Tilt event detection status on Z-negative axis. **/
  uint8_t wrist_tilt_ia_zpos  : 1;  /**< Absolute Wrist Tilt event detection status on Z-positive axis. **/
  uint8_t wrist_tilt_ia_yneg  : 1;  /**< Absolute Wrist Tilt event detection status on Y-negative axis. **/
  uint8_t wrist_tilt_ia_ypos  : 1;  /**< Absolute Wrist Tilt event detection status on Y-positive axis. **/
  uint8_t wrist_tilt_ia_xneg  : 1;  /**< Absolute Wrist Tilt event detection status on X-negative axis. **/
  uint8_t wrist_tilt_ia_xpos  : 1;  /**< Absolute Wrist Tilt event detection status on X-positive axis. **/
} lsm6dsm_wrist_tilt_ia_t;

typedef struct {
  uint8_t lir                 : 1;  /**< Latched Interrupt. 0: interrupt request not latched **/
  uint8_t tap_z_en            : 1;  /**< Enable Z direction in tap recognition. 0: Z direction disabled  **/
  uint8_t tap_y_en            : 1;  /**< Enable Y direction in tap recognition. 0: Y direction disabled  **/
  uint8_t tap_x_en            : 1;  /**< Enable X direction in tap recognition. 0: X direction disabled **/
  uint8_t slope_fds           : 1;  /**< HPF or SLOPE filter sel. on wake-up and Activity/Inactivity. @ref lsm6dsm_hpf_slope_t **/
  uint8_t inact_en            : 2;  /**< Enable inactivity function. @ref lsm6dsm_inact_en_t **/
  uint8_t interrupts_enable   : 1;  /**< Enable interrupts (6D/4D, free-fall, wake-up, tap, inactivity). 0: disabled **/
} lsm6dsm_tap_cfg_t;

typedef struct {
  uint8_t tap_ths             : 5;  /**< Threshold for tap recognition. 1 LSb corresponds to FS_XL/2 **/
  uint8_t sixd_ths            : 2;  /**< Threshold for 4D/6D function. 80 deg - (10deg * LSB) **/
  uint8_t d4d_disable         : 1;  /**< 4D orientation detection enable. Z-axis position dect disabled. 0:enabled **/
} lsm6dsm_tap_ths_6d_t;

typedef struct {
  uint8_t shock               : 2;  /**< Maximum duration of overthreshold event.
                                          default: 00 =>  4*ODR_XL time
                                          else:  1LSb = 8*ODR_XL **/
  uint8_t quiet               : 2;  /**< Expected quiet time after a tap detection. 
                                          default: 00 => 2*ODR_XL time
                                          else  1LSb = 4*ODR_XL **/
  uint8_t dur                 : 4;  /**< Duration of maximum time gap for double tap recognition. 
                                          double tap recognition enabled => register expresses the maximum time
                                          between two consecutive detected taps to determine double tap event. 
                                          default:  0000b => 16*ODR_XL time. 
                                          else => 1LSb = 32*ODR_XL time. **/
} lsm6dsm_int_dur2_t;

typedef struct {
  uint8_t wk_ths              : 6;  /**< Threshold for wakeup. 1 LSb = FS_XL/2**/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t single_double_tap   : 1;  /**< Single/double-tap event enable. 0: only single-tap event enabled  **/
} lsm6dsm_wake_up_ths_t;

typedef struct {
  uint8_t sleep_dur           : 4;  /**< Duration to go in sleep mode. 0 => 16 ODR, 1 LSb = 512 ODR **/
  uint8_t timer_hr            : 1;  /**< Timestamp register resolution setting @ref lsm6dsm_timer_hr_t**/
  uint8_t wake_dur            : 2;  /**< Wake up duration event. 1LSB = 1 ODR_time **/
  uint8_t ff_dur              : 1;  /**< Free fall duration event. 1 LSB = 1 ODR_time **/
} lsm6dsm_wake_up_dur_t;

typedef struct {                    /**< Wake up duration event: 0 **/
  uint8_t ff_ths              : 3;  /**< Free fall threshold setting. @ref lsm6dsm_ff_ths_t  **/
  uint8_t ff_dur              : 5;  /**< Free-fall duration event. @ref WAKE_UP_DUR (5Ch)  **/
} lsm6dsm_free_fall_t;

typedef struct {
  uint8_t int1_timer          : 1;  /**< Routing of end counter event of timer on INT1. 0: routing disabled **/
  uint8_t int1_tilt           : 1;  /**< Routing of tilt event on INT1. 0: routing disabled **/
  uint8_t int1_6d             : 1;  /**< Routing of 6D event on INT1. 0: routing disabled **/
  uint8_t int1_double_tap     : 1;  /**< Routing of tap event on INT1. 0: routing disabled **/
  uint8_t int1_ff             : 1;  /**< Routing of free-fall event on INT1. 0: routing disabled **/
  uint8_t int1_wu             : 1;  /**< Routing of wakeup event on INT1. 0: routing disabled **/
  uint8_t int1_single_tap     : 1;  /**< Single-tap recognition routing on INT1. 0: routing disabled **/
  uint8_t int1_inact_state    : 1;  /**< Routing on INT1 of inactivity mode. 0: routing disabled **/
} lsm6dsm_md1_cfg_t;

typedef struct {
  uint8_t int2_iron           : 1;  /**< Routing of soft-iron/hard-iron algo end event on INT2. 0: routing disabled  **/
  uint8_t int2_tilt           : 1;  /**< Routing of tilt event on INT2. 0: routing disabled  **/
  uint8_t int2_6d             : 1;  /**< Routing of 6D event on INT2.   0: routing disabled **/
  uint8_t int2_double_tap     : 1;  /**< Routing of tap event on INT2.  0: routing disabled **/
  uint8_t int2_ff             : 1;  /**< Routing of free-fall event on INT2. 0: routing disabled **/
  uint8_t int2_wu             : 1;  /**< Routing of wakeup event on INT2.    0: routing disabled **/
  uint8_t int2_single_tap     : 1;  /**< Single-tap recognition routing on INT2. 0: routing disabled **/
  uint8_t int2_inact_state    : 1;  /**< Routing on INT2 of inactivity mode. 0: routing disabled  **/
} lsm6dsm_md2_cfg_t;

typedef struct {
  uint8_t master_cmd_code     : 8;  /**< Master command code used for stamping for sensor sync: 0 **/
} lsm6dsm_master_cmd_code_t;

typedef struct {
  uint8_t sens_sync_spi_errc  : 8;  /**< Error code used for sensor synchronization: 0 **/
} lsm6dsm_sens_sync_spi_errc_t;

typedef struct {
  uint8_t                     : 6;  /**< RESERVED **/
  uint8_t lvl2_ois            : 1;  /**< Enables level-sensitive latched mode on the OIS chain. **/
  uint8_t int2_drdy_ois       : 1;  /**< Enables the OIS chain DRDY on the INT2 pad. setting priority > other INT2 **/
} lsm6dsm_int_ois_t;


typedef struct {
  uint8_t ois_en_spi2         : 1;  /**< Enables OIS chain data processing for gyro in Mode 3 and Mode 4 **/
  uint8_t fs_g_ois            : 3;  /**< Gyroscope OIS chain full-scale selection. @ref lsm6dsm_fs_g_ois_t **/  
  uint8_t mode4_en            : 1;  /**< Enables accelerometer OIS chain if OIS_EN_SPI2 = 1 **/
  uint8_t sim_ois             : 1;  /**< SPI2 3- or 4-wire mode. 0: 4-wire SPI2 **/
  uint8_t lvl1_ois            : 1;  /**< Enables level-sensitive trigger mode on OIS chain. **/
  uint8_t ble_ois             : 1;  /**< Big/Little Endian data selection. 0: out LSbyte at lower reg. address  **/
} lsm6dsm_ctrl1_ois_t;

typedef struct {
  uint8_t hp_en_ois           : 1;  /**< Enable gyroscope's OIS chain HPF. Filter available on the OIS chain only if HP_EN_G  **/
  uint8_t ftype_ois           : 2;  /**< Gyroscope's digital LPF1 filter bandwidth selection. @ref lsm6dsm_ftype_ois_t**/
  uint8_t                     : 1;  /**< RESERVED **/
  uint8_t hpm_ois             : 2;  /**< Gyro OIS chain dig. high-pass filter cutoff. @ref lsm6dsm_hpm_ois_t **/
  uint8_t                     : 2;  /**< RESERVED **/
} lsm6dsm_ctrl2_ois_t;

typedef struct {
  uint8_t st_ois_clampdis     : 1;  /**< Gyro OIS chain clamp disable 0: All gyro OIS chain outputs = 8000h Self Test**/
  uint8_t st_ois              : 2;  /**< Gyroscope OIS chain self-test selection  **/
  uint8_t filter_xl_conf_ois  : 2;  /**< Accelerometer OIS channel bandwidth selection **/
  uint8_t fs_xl_ois           : 2;  /**< Accelerometer OIS channel full-scale selection.  **/
  uint8_t den_lh_ois          : 1;  /**< Polarity of DEN signal on OIS chain. 0: DEN pin is active-low **/
} lsm6dsm_ctrl3_ois_t;


typedef struct {
  uint8_t rw_0                : 1;  /**< Read/write operation on Sensor1. 0: write operation**/
  uint8_t slave0_add          : 7;  /**< I2C slave address of Sensor1 **/
} lsm6dsm_slv0_add_t;

typedef struct {
  uint8_t slave0_numop        : 3;  /**< Number of read operations on Sensor1. **/
  uint8_t src_mode            : 1;  /**< Source mode conditioned read. 0: source mode read disabled **/
  uint8_t aux_sens_on         : 2;  /**< Number of external sensors to be read by sensor hub. 00: one sensor **/
  uint8_t slave0_rate         : 2;  /**< Decimation of read op on Sensor1 starting from sensor hub trigger. **/
} lsm6dsm_slave0_config_t;

typedef struct {
  uint8_t r_1                 : 1;  /**< Read operation on Sensor2 enable. 0: read operation disabled **/
  uint8_t slave1_add          : 7;  /**< I2C slave address of Sensor2 **/
} lsm6dsm_slv1_add_t;

typedef struct {
  uint8_t slave1_numop        : 3;  /**< Number of read operations on Sensor2 **/
  uint8_t                     : 2;  /**< RESERVED **/
  uint8_t write_once          : 1;  /**< Slave 1 write operation is performed only at the first sensor hub cycle. **/
  uint8_t slave1_rate         : 2;  /**< Decimation of read op on Sensor2 starting from sensor hub trigger. **/
} lsm6dsm_slave1_config_t;

typedef struct {
  uint8_t r_2                 : 1;  /**< Read operation on Sensor3 enable. 0: read operation disabled **/
  uint8_t slave2_add          : 7;  /**< I2C slave address of Sensor3 **/
} lsm6dsm_slv2_add_t;

typedef struct {
  uint8_t slave2_numop        : 3;  /**< Number of read operations on Sensor3 **/
  uint8_t                     : 3;  /**< RESERVED **/
  uint8_t slave2_rate         : 2;  /**< Decimation of read op on Sensor3 starting from sensor hub trigger. **/
} lsm6dsm_slave2_config_t;

typedef struct {
  uint8_t r_3                 : 1;  /**< Read operation on Sensor4 enable. 0: read operation disabled **/
  uint8_t slave3_add          : 7;  /**< I2C slave address of Sensor4 **/
} lsm6dsm_slv3_add_t;

typedef struct {
  uint8_t slave3_numop        : 3;  /**< Number of read operations on Sensor4 **/
  uint8_t                     : 3;  /**< RESERVED **/
  uint8_t slave3_rate         : 2;  /**< Decimation of read op on Sensor4 starting from sensor hub trigger. **/
} lsm6dsm_slave3_config_t;


typedef struct {
  uint8_t ths_min             : 5;  /**< Minimum threshold to detect a peak. 10b default.  **/
  uint8_t                     : 2;  /**< RESERVED **/
  uint8_t pedo_fs             : 1;  /**< Pedometer data elaboration at 2g/4g. (0: 2g, 1: 4g)**/
} lsm6dsm_config_pedo_ths_min_t;

typedef struct {
  uint8_t deb_step            : 3;  /**< Debounce threshold. Min. steps to increment step counter. Default: 110 **/
  uint8_t deb_time            : 5;  /**< Debounce time. Time between steps > DEB_TIME*80ms => reactiv. 
                                          Debouncer Default value: 01101 **/
} lsm6dsm_pedo_deb_reg_t;

typedef struct {
  uint8_t                      : 2;  /**< RESERVED **/
  uint8_t  wrist_tilt_msk_zneg : 1;  /**< Absolute wrist tilt negative Z-axis enable. Default: 0 **/
  uint8_t  wrist_tilt_msk_zpos : 1;  /**< Absolute wrist tilt positive Z-axis enable. Default: 0 **/
  uint8_t  wrist_tilt_msk_yneg : 1;  /**< Absolute wrist tilt negative Y-axis enable. Default: 0 **/
  uint8_t  wrist_tilt_msk_ypos : 1;  /**< Absolute wrist tilt positive Y-axis enable. Default: 0 **/
  uint8_t  wrist_tilt_msk_xneg : 1;  /**< Absolute wrist tilt negative X-axis enable. Default: 1 **/
  uint8_t  wrist_tilt_msk_xpos : 1;  /**< Absolute wrist tilt positive X-axis enable. Default: 1 **/
} lsm6dsm_a_wrist_tilt_mask_t;
/*! @} */

#endif /* LSM6DSM_REG_H */
